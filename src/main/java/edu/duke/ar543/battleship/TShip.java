package edu.duke.ar543.battleship;
import java.util.HashMap;

public class TShip<T> extends BasicShip<T> {
  private char orientation;
  private HashMap<Integer, Coordinate> deltaOrientationUp;
  private HashMap<Integer, Coordinate> deltaOrientationRight;
  private HashMap<Integer, Coordinate> deltaOrientationDown;
  private HashMap<Integer, Coordinate> deltaOrientationLeft;

  static HashMap<Integer, Coordinate> makeCoords(Coordinate upperLeft, char orientation) {
    HashMap<Integer, Coordinate> shipCoords = new HashMap<Integer, Coordinate>();
    if (orientation == 'U') {
      shipCoords.put(1, new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 1));
      shipCoords.put(2, new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn()));
      shipCoords.put(3, new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1));
      shipCoords.put(4, new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 2));
    }
    else if (orientation == 'R') {
      shipCoords.put(1, new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1));
      shipCoords.put(2, new Coordinate(upperLeft.getRow(), upperLeft.getColumn()));
      shipCoords.put(3, new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn()));
      shipCoords.put(4, new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn()));
    }
    else if (orientation == 'D') {
      shipCoords.put(1, new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1));
      shipCoords.put(2, new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 2));
      shipCoords.put(3, new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 1));
      shipCoords.put(4, new Coordinate(upperLeft.getRow(), upperLeft.getColumn()));
    }
    else if (orientation == 'L') {
      shipCoords.put(1, new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn()));
      shipCoords.put(2, new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn() + 1));
      shipCoords.put(3, new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1));
      shipCoords.put(4, new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 1));
    }
    else {
      throw new IllegalArgumentException("Invalid orientation for TShip.");
    }
    return shipCoords;
  }

  /**
  public HashMap<Integer, Coordinate> getDeltaUp() {
    return deltaOrientationUp;
  }

  public HashMap<Integer, Coordinate> getDeltaRight() {
    return deltaOrientationRight;
  }
  
  public HashMap<Integer, Coordinate> getDeltaDown() {
    return deltaOrientationDown;
  }

  public HashMap<Integer, Coordinate> getDeltaLeft() {
    return deltaOrientationLeft;
  }
  */
  public TShip(String name,
               Coordinate upperLeft,
               char orientation,
               ShipDisplayInfo<T> myDisplayInfo,
               ShipDisplayInfo<T> enemyDisplayInfo) {
    super(name, makeCoords(upperLeft, orientation), myDisplayInfo, enemyDisplayInfo);
    this.orientation = orientation;

    // HARD-CODED values. Should not be changed!
    deltaOrientationUp = new HashMap<Integer, Coordinate>();
    deltaOrientationUp.put(1, new Coordinate(0, 1));
    deltaOrientationUp.put(2, new Coordinate(1, 0));
    deltaOrientationUp.put(3, new Coordinate(1, 1));
    deltaOrientationUp.put(4, new Coordinate(1, 2));

    deltaOrientationRight = new HashMap<Integer, Coordinate>();
    deltaOrientationRight.put(1, new Coordinate(1, 1));
    deltaOrientationRight.put(2, new Coordinate(0, 0));
    deltaOrientationRight.put(3, new Coordinate(1, 0));
    deltaOrientationRight.put(4, new Coordinate(2, 0));

    deltaOrientationDown = new HashMap<Integer, Coordinate>();
    deltaOrientationDown.put(1, new Coordinate(1, 1));
    deltaOrientationDown.put(2, new Coordinate(0, 2));
    deltaOrientationDown.put(3, new Coordinate(0, 1));
    deltaOrientationDown.put(4, new Coordinate(0, 0));

    deltaOrientationLeft = new HashMap<Integer, Coordinate>();
    deltaOrientationLeft.put(1, new Coordinate(1, 0));
    deltaOrientationLeft.put(2, new Coordinate(2, 1));
    deltaOrientationLeft.put(3, new Coordinate(1, 1));
    deltaOrientationLeft.put(4, new Coordinate(0, 1));
  }

  public TShip(String name,
               Coordinate upperLeft,
               char orientation,
               HashMap<Integer, Boolean> pieceToHit,
               ShipDisplayInfo<T> myDisplayInfo,
               ShipDisplayInfo<T> enemyDisplayInfo) {
    super(name, makeCoords(upperLeft, orientation), pieceToHit, myDisplayInfo, enemyDisplayInfo);
    this.orientation = orientation;

    // HARD-CODED values. Should not be changed!
    deltaOrientationUp = new HashMap<Integer, Coordinate>();
    deltaOrientationUp.put(1, new Coordinate(0, 1));
    deltaOrientationUp.put(2, new Coordinate(1, 0));
    deltaOrientationUp.put(3, new Coordinate(1, 1));
    deltaOrientationUp.put(4, new Coordinate(1, 2));

    deltaOrientationRight = new HashMap<Integer, Coordinate>();
    deltaOrientationRight.put(1, new Coordinate(1, 1));
    deltaOrientationRight.put(2, new Coordinate(0, 0));
    deltaOrientationRight.put(3, new Coordinate(1, 0));
    deltaOrientationRight.put(4, new Coordinate(2, 0));

    deltaOrientationDown = new HashMap<Integer, Coordinate>();
    deltaOrientationDown.put(1, new Coordinate(1, 1));
    deltaOrientationDown.put(2, new Coordinate(0, 2));
    deltaOrientationDown.put(3, new Coordinate(0, 1));
    deltaOrientationDown.put(4, new Coordinate(0, 0));

    deltaOrientationLeft = new HashMap<Integer, Coordinate>();
    deltaOrientationLeft.put(1, new Coordinate(1, 0));
    deltaOrientationLeft.put(2, new Coordinate(2, 1));
    deltaOrientationLeft.put(3, new Coordinate(1, 1));
    deltaOrientationLeft.put(4, new Coordinate(0, 1));
  }

  public TShip(String name,
               Coordinate upperLeft,
               char orientation,
               T data,
               T onHit) {
    this(name,
         upperLeft,
         orientation,
         new SimpleShipDisplayInfo<T>(data, onHit),
         new SimpleShipDisplayInfo<T>(null, data));
  }
  
  /**
  public char getOrientation() {
    return orientation;
  }
  */  
  
  /** Returns the upper left coordinate of the ship
   */
  public Coordinate getStartingCoordinate() {
    if (orientation == 'U') {
      return pieceToCoord.get(1).add(new Coordinate(0,-1));
    }
    else if (orientation == 'R') {
      return pieceToCoord.get(2);
    }
    else if (orientation == 'D') {
      return pieceToCoord.get(4);
    }
    return pieceToCoord.get(1).add(new Coordinate(-1,0));
  }
  
  public String getName() {
    return name;
  }
}



