package edu.duke.ar543.battleship;

import java.util.HashMap;

public class RectangleShip<T> extends BasicShip<T> {  
  static HashMap<Integer, Coordinate> makeCoords(Coordinate upperLeft, int width, int height) {
    HashMap<Integer, Coordinate> shipCoords = new HashMap<Integer, Coordinate>();
    int pieceId = 1;
    for (int row_offset = 0; row_offset < height; row_offset++) {
       for (int col_offset = 0; col_offset < width; col_offset++) {         
         shipCoords.put(pieceId, new Coordinate(upperLeft.getRow() + row_offset,
                                       upperLeft.getColumn() + col_offset));
         pieceId += 1;
       }
    }
    return shipCoords;
  }

  public RectangleShip(String name,
                       Coordinate upperLeft,
                       int w,
                       int h,
                       ShipDisplayInfo<T> myDisplayInfo,
                       ShipDisplayInfo<T> enemyDisplayInfo) {
    super(name, makeCoords(upperLeft, w, h), myDisplayInfo, enemyDisplayInfo);
    width = w;
    height = h;
  }

  public RectangleShip(String name,
                       Coordinate upperLeft,
                       int w,
                       int h,
                       HashMap<Integer, Boolean> pieceToHit,
                       ShipDisplayInfo<T> myDisplayInfo,
                       ShipDisplayInfo<T> enemyDisplayInfo) {
    super(name, makeCoords(upperLeft, w, h), pieceToHit, myDisplayInfo, enemyDisplayInfo);
    width = w;
    height = h;
  }

  public RectangleShip(String name, Coordinate upperLeft, int width, int height, T data, T onHit) {
    this(name,
         upperLeft,
         width,
         height,
         new SimpleShipDisplayInfo<T>(data, onHit),
         new SimpleShipDisplayInfo<T>(null, data));
  }

  public RectangleShip(String name,
                       Coordinate upperLeft,
                       int w,
                       int h,
                       HashMap<Integer, Boolean> pieceToHit,
                       T data, T onHit) {
    this(name,
         upperLeft,
         w,
         h,
         pieceToHit,
         new SimpleShipDisplayInfo<T> (data, onHit),
         new SimpleShipDisplayInfo<T> (null, data));
  }
  
  public RectangleShip(Coordinate upperLeft, T data, T onHit) {
    this("testship",
         upperLeft,
         1,
         1,
         new SimpleShipDisplayInfo<>(data, onHit),
         new SimpleShipDisplayInfo<T>(null, data));
  }

  @Override
  public Coordinate getStartingCoordinate() {
    return pieceToCoord.get(1);
  }
}









