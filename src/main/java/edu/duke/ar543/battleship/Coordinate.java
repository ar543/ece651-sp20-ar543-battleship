package edu.duke.ar543.battleship;

public class Coordinate {
  private final int row;
  private final int column;

  public Coordinate (int r, int c) {
    this.row = r;
    this.column = c;
  }

  /**
    * Initializes row and column based on a string input where row
    * is a letter and column is an integer. E.g. "A4" gets initialized
    * to row=0 and column=4.
   */
  public Coordinate(String descr) throws IllegalArgumentException {
    String lowerDescr = descr.toLowerCase();
    String wrongCoordinateError = "Coordinate must be of length two." +
      "The first character specifies the row (A-Z)." +
      "The second letter must specify a column (0-9).\n";

    // assert length = 2
    if (lowerDescr.length() != 2) {
      throw new IllegalArgumentException(
          "Invalid coordinate passed!\n" +
          wrongCoordinateError +
          "Coordinate passed = " +
          descr +
          "\n");
    }

    boolean isAlphabet = lowerDescr.charAt(0) >= 'a' && lowerDescr.charAt(0) <= 'z';
    if (!isAlphabet) {
      throw new IllegalArgumentException("Invalid row! Row must be an alphabet from A to Z.\n" +
                                         wrongCoordinateError +
                                         "Coordinate passed = " +
                                         descr +
                                         "\n");
    }

    boolean isDigit = lowerDescr.charAt(1) >= '0' &&
      lowerDescr.charAt(1) <= '9';
    if (!isDigit) {
      throw new IllegalArgumentException("Invalid column! Column must be a digit between 0 and 9 (inclusive of both)" +
                                         "Coordinate passed = " +
                                         descr +
                                         "\n");
    }

    // extract row and column
    this.row = (int) (lowerDescr.charAt(0) - 97); // subtract ascii value of 'a' from char between 'a' to 'z'
    this.column = Character.getNumericValue(lowerDescr.charAt(1));
  }
  
  public int getRow() {
    return row;
  }

  public int getColumn() {
    return column;
  }

  /*
 * Adds two coordinates by adding corresponding rows and columns.
 */
  public Coordinate add(Coordinate c) {
    return new Coordinate(row + c.getRow(), column + c.getColumn());
  }

  @Override
  public boolean equals(Object o) {
    if (o != null && o.getClass().equals(getClass())) {
      Coordinate other = (Coordinate) o;
      return this.row == other.getRow() && this.column == other.getColumn();
    }
    return false;
  }

  @Override
  public String toString() {
    return "(" + row + ", " + column + ")";
  }

  @Override
  public int hashCode() {
    return toString().hashCode();
  }
}













