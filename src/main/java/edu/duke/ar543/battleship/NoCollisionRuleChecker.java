package edu.duke.ar543.battleship;
import java.util.Iterator;

public class NoCollisionRuleChecker<T> extends PlacementRuleChecker<T> {

  public NoCollisionRuleChecker(PlacementRuleChecker<T> next) {
    super(next);
  }
	@Override
	protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
    Iterable<Coordinate> shipCoords = theShip.getCoordinates();
    Iterator<Coordinate> shipCoordsIt = shipCoords.iterator();
    while(shipCoordsIt.hasNext()) {
      Coordinate c = shipCoordsIt.next();
      if (theBoard.whatIsAtForSelf(c) != null) {
        String errMessage = "That placement is invalid: the ship overlaps another ship.\n";
        return errMessage;
      }
    }
    return null;
	}

}












