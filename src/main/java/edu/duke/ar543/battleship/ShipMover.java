package edu.duke.ar543.battleship;

/*
 * Moves a ship from one square to another on a Board.
 */
public abstract class ShipMover<T> {
  protected final Board<T> board;

  public ShipMover (Board<T> b) {
    this.board = b;
  }

  /*
  * Moves given ship to the placement specified.
  * @throws if placement is out of bounds of the board, or if ship overlaps another ship at `to`.  
  */
  public abstract boolean moveShip(BasicShip<T> ship, Placement to) throws IllegalArgumentException;
}








