package edu.duke.ar543.battleship;

import java.util.HashMap;

/*
 * A simple 2d board that serves as the interface for BattleshipBoard class.
 */
public interface Board<T> {

  // returns width of the board
  public int getWidth();

  // returns height of the board
  public int getHeight();

  /*
  * Adds a ship to the board
  *@returns null if ship is added succesfully, else returns an error message.  
  */
  public String tryAddShip (Ship<T> toAdd);

  /*
 * Returns content for a player themself at a particular square on the board.
 */
  public T whatIsAtForSelf(Coordinate where);

  /*
 * Returns content for enemy at a square.
 */
  public T whatIsAtForEnemy(Coordinate where);

  /*
  * Mark damage to a ship on the board.
  *@returns ship if c belongs to a ship else returns null.  
  */
  public Ship<T> fireAt(Coordinate c);

  /*
 * Whether or not all ships have sunk on the board
 */
  public boolean allShipsHaveSunk();

  /*
 * Removes a ship from the board.
 */
  public void deleteShip(Coordinate where);

    /**
   * Sonar scan: returns number of squares occupied by each ship 
   * present around a coordinate C in the following pattern:
   *                 *
   *                ***
   *               *****
   *              ***C***
   *               *****
   *                ***
   *                 *
   * @param where is the center coordinate C around which to find ships
   * @returns a hashmap of ship name and the number of squares occupied by it.
   * @throws IllegalArgumentException if C is not part of the battleship  
   */  
   public HashMap<String, Integer> sonar(int range, Coordinate where) throws IllegalArgumentException;

  //public Iterable<Ship<T>> getShips();

  /*
 * Returns a ship present at a given coordinate. Returns null if no ship is present.
 */
  public Ship<T> getShipAtCoordinate(Coordinate where);
}













