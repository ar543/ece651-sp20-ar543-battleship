package edu.duke.ar543.battleship;

/*
 * Allows a player to mention a square on battleship board.
 * Placement in a three character long string such as "A3V" or "F9D"
 * The first and character are the row and column of the ship to be placed on a 2D board.
 * The third character tells the orientation of the ship to be placed. 
 */
public class Placement {
  private final Coordinate coord;
  private final char orientation;

  /*
  * Initializes orientation of ship.
  * @returns a character that describes the orientation.  
  * @throws when orientation is not one of (v)ertical, (h)orizontal,
  * (u)p, (d)own, (r)ight, or (l)eft.  
  */
  private char initializeOrientation(char c) throws IllegalArgumentException {
    char orientation;
    if (c == 'V' || c == 'v') {
      orientation = 'V';
    }
    else if (c == 'H' || c == 'h') {
      orientation = 'H';
    }
    else if (c == 'U' || c == 'u') {
      orientation = 'U';
    }
    else if (c == 'R' || c == 'r') {
      orientation = 'R';
    }
    else if (c == 'D' || c == 'd') {
      orientation = 'D';
    }
    else if (c == 'L' || c == 'l') {
      orientation = 'L';
    }
    else {
      throw new IllegalArgumentException("That placement is invalid: it does not have the correct format.\n");
    }
    return orientation;
  }
  
  // simple constructor to inilialize both fields
  public Placement (Coordinate c, char orient) throws IllegalArgumentException {
    this.coord = c;
    this.orientation = initializeOrientation(orient);
  }

  public Placement (String descr) throws IllegalArgumentException {
    if (descr == null || descr.length() != 3) {
      throw new IllegalArgumentException("That placement is invalid: it does not have the correct format.\n");
    }
    
    Coordinate c = new Coordinate(descr.substring(0,2));
    this.coord = c;

    this.orientation = initializeOrientation(descr.charAt(2));
  }
  
  public Coordinate getCoordinate() {
    return coord;
  }

  public char getOrientation() {
    return orientation;
  }

  public String toString() {
    return "[" + coord.toString() + ", " + orientation + "]";
  }

  public int hashCode() {
    return toString().hashCode();
  }

  public boolean equals(Object o) {
    if (o != null && o.getClass().equals(getClass())) {
      Placement other = (Placement) o;
      Coordinate otherCoord = other.getCoordinate();
      return coord.equals(otherCoord) && orientation == other.getOrientation();
    }
    return false;
  }
}













