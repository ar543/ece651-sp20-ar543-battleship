package edu.duke.ar543.battleship;

import java.util.HashMap;

public class ZShip<T> extends BasicShip<T> {
  private char orientation;
  private HashMap<Integer, Coordinate> deltaOrientationUp;
  private HashMap<Integer, Coordinate> deltaOrientationRight;
  private HashMap<Integer, Coordinate> deltaOrientationDown;
  private HashMap<Integer, Coordinate> deltaOrientationLeft;

  static HashMap<Integer, Coordinate> makeCoords(Coordinate upperLeft, char orientation) {
    HashMap<Integer, Coordinate> shipCoords = new HashMap<Integer, Coordinate>();
    if (orientation == 'U' || orientation == 'u') {
      shipCoords.put(1, new Coordinate(upperLeft.getRow(), upperLeft.getColumn()));
      shipCoords.put(2, new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn()));
      shipCoords.put(3, new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn()));
      shipCoords.put(4, new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn() + 1));
      shipCoords.put(5, new Coordinate(upperLeft.getRow() + 3, upperLeft.getColumn() + 1));
      shipCoords.put(6, new Coordinate(upperLeft.getRow() + 4, upperLeft.getColumn() + 1));
    }
    else if (orientation == 'R' || orientation == 'r') {
      shipCoords.put(1, new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 4));
      shipCoords.put(2, new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 3));
      shipCoords.put(3, new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 2));
      shipCoords.put(4, new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 2));
      shipCoords.put(5, new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1));
      shipCoords.put(6, new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn()));
    }
    else if (orientation == 'D' || orientation == 'd') {
      shipCoords.put(1, new Coordinate(upperLeft.getRow() + 4, upperLeft.getColumn()));
      shipCoords.put(2, new Coordinate(upperLeft.getRow() + 3, upperLeft.getColumn()));
      shipCoords.put(3, new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn()));
      shipCoords.put(4, new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn() + 1));
      shipCoords.put(5, new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1));
      shipCoords.put(6, new Coordinate(upperLeft.getRow() + 0, upperLeft.getColumn() + 1));
    }
    else if (orientation == 'L' || orientation == 'l') {
      shipCoords.put(1, new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 4));
      shipCoords.put(2, new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 3));
      shipCoords.put(3, new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 2));
      shipCoords.put(4, new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 2));
      shipCoords.put(5, new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 1));
      shipCoords.put(6, new Coordinate(upperLeft.getRow(), upperLeft.getColumn()));
    }
    else {
      throw new IllegalArgumentException("Invalid orientation for ZShip.");
    }
    return shipCoords;
  }

  public ZShip(String name,
               Coordinate upperLeft,
               char orientation,
               ShipDisplayInfo<T> myDisplayInfo,
               ShipDisplayInfo<T> enemyDisplayInfo) {
    super(name, makeCoords(upperLeft, orientation), myDisplayInfo, enemyDisplayInfo);
    this.orientation = orientation;

    // HARD-CODED values. Should not be changed!
    deltaOrientationUp = new HashMap<Integer, Coordinate>();
    deltaOrientationUp.put(1, new Coordinate(0, 0));
    deltaOrientationUp.put(2, new Coordinate(1, 0));
    deltaOrientationUp.put(3, new Coordinate(2, 0));
    deltaOrientationUp.put(4, new Coordinate(2, 1));
    deltaOrientationUp.put(5, new Coordinate(3, 1));
    deltaOrientationUp.put(6, new Coordinate(4, 1));

    deltaOrientationRight = new HashMap<Integer, Coordinate>();
    deltaOrientationRight.put(1, new Coordinate(0, 4));
    deltaOrientationRight.put(2, new Coordinate(0, 3));
    deltaOrientationRight.put(3, new Coordinate(0, 2));
    deltaOrientationRight.put(4, new Coordinate(1, 2));
    deltaOrientationRight.put(5, new Coordinate(1, 1));
    deltaOrientationRight.put(6, new Coordinate(1, 0));

    deltaOrientationDown = new HashMap<Integer, Coordinate>();
    deltaOrientationDown.put(1, new Coordinate(4, 0));
    deltaOrientationDown.put(2, new Coordinate(3, 0));
    deltaOrientationDown.put(3, new Coordinate(2, 0));
    deltaOrientationDown.put(4, new Coordinate(2, 1));
    deltaOrientationDown.put(5, new Coordinate(1, 1));
    deltaOrientationDown.put(6, new Coordinate(0, 1));

    deltaOrientationLeft = new HashMap<Integer, Coordinate>();
    deltaOrientationLeft.put(1, new Coordinate(1, 4));
    deltaOrientationLeft.put(2, new Coordinate(1, 3));
    deltaOrientationLeft.put(3, new Coordinate(1, 2));
    deltaOrientationLeft.put(4, new Coordinate(0, 2));
    deltaOrientationLeft.put(5, new Coordinate(0, 1));
    deltaOrientationLeft.put(6, new Coordinate(0, 0));
  }

  public ZShip(String name,
               Coordinate upperLeft,
               char orientation,
               HashMap<Integer, Boolean> pieceToHit,
               ShipDisplayInfo<T> myDisplayInfo,
               ShipDisplayInfo<T> enemyDisplayInfo) {
    super(name, makeCoords(upperLeft, orientation), pieceToHit, myDisplayInfo, enemyDisplayInfo);
    this.orientation = orientation;

    // HARD-CODED values. Should not be changed!
    deltaOrientationUp = new HashMap<Integer, Coordinate>();
    deltaOrientationUp.put(1, new Coordinate(0, 0));
    deltaOrientationUp.put(2, new Coordinate(1, 0));
    deltaOrientationUp.put(3, new Coordinate(2, 0));
    deltaOrientationUp.put(4, new Coordinate(2, 1));
    deltaOrientationUp.put(5, new Coordinate(3, 1));
    deltaOrientationUp.put(6, new Coordinate(4, 1));

    deltaOrientationRight = new HashMap<Integer, Coordinate>();
    deltaOrientationRight.put(1, new Coordinate(0, 4));
    deltaOrientationRight.put(2, new Coordinate(0, 3));
    deltaOrientationRight.put(3, new Coordinate(0, 2));
    deltaOrientationRight.put(4, new Coordinate(1, 2));
    deltaOrientationRight.put(5, new Coordinate(1, 1));
    deltaOrientationRight.put(6, new Coordinate(1, 0));

    deltaOrientationDown = new HashMap<Integer, Coordinate>();
    deltaOrientationDown.put(1, new Coordinate(4, 0));
    deltaOrientationDown.put(2, new Coordinate(3, 0));
    deltaOrientationDown.put(3, new Coordinate(2, 0));
    deltaOrientationDown.put(4, new Coordinate(2, 1));
    deltaOrientationDown.put(5, new Coordinate(1, 1));
    deltaOrientationDown.put(6, new Coordinate(0, 1));

    deltaOrientationLeft = new HashMap<Integer, Coordinate>();
    deltaOrientationLeft.put(1, new Coordinate(1, 4));
    deltaOrientationLeft.put(2, new Coordinate(1, 3));
    deltaOrientationLeft.put(3, new Coordinate(1, 2));
    deltaOrientationLeft.put(4, new Coordinate(0, 2));
    deltaOrientationLeft.put(5, new Coordinate(0, 1));
    deltaOrientationLeft.put(6, new Coordinate(0, 0));
  }

  public ZShip(String name,
               Coordinate upperLeft,
               char orientation,
               T data,
               T onHit) {
    this(name,
         upperLeft,
         orientation,
         new SimpleShipDisplayInfo<T>(data, onHit),
         new SimpleShipDisplayInfo<T>(null, data));
  }

  /**
  public char getOrientation() {
    return orientation;
  }
  */  
  
  /** Returns the upper left coordinate of the ship
   */
  public Coordinate getStartingCoordinate() {
    if (orientation == 'U') {
      return pieceToCoord.get(1);
    }
    else if (orientation == 'R') {
      return pieceToCoord.get(6).add(new Coordinate(-1, 0));
    }
    else if (orientation == 'D') {
      return pieceToCoord.get(6).add(new Coordinate(0, -1));
    }
  return pieceToCoord.get(6);
  }
  
  public String getName() {
    return name;
  }
}

