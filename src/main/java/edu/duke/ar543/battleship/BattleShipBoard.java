package edu.duke.ar543.battleship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/*
 * A 2D board where battleship is played.
 */
public class BattleShipBoard<T> implements Board<T> {
  private final int width;
  private final int height;
  private final ArrayList<Ship<T>> myShips;
  private final PlacementRuleChecker<T> placementChecker;
  private final HashMap<Coordinate, T> strikes;
  private HashSet<Coordinate> enemyMisses;
  final T missInfo;

  public int getWidth() {
    return this.width;
  }

  public int getHeight() {
    return this.height;
  }

  public BattleShipBoard(int width, int height, PlacementRuleChecker<T> prc, T missInfo) {
    /**
     * Constructs a BattleShipBoard with the specified width and height
     * 
     * @param w is the width of the newly constructed board
     * @param h is the height of the newly constructed board
     * @throws IllegalArgumentException if the width or height are less than or
     *                                  equal to zero
     */

    if (width <= 0) {
      throw new IllegalArgumentException("BattleShipBoard's width must be positive but is " + width);
    }
    if (height <= 0) {
      throw new IllegalArgumentException("BattleShipBoard's height must be positive but is " + height);
    }
    this.width = width;
    this.height = height;
    this.myShips = new ArrayList<Ship<T>>();
    this.placementChecker = prc;
    this.enemyMisses = new HashSet<Coordinate>();
    this.missInfo = missInfo;
    this.strikes = new HashMap<Coordinate, T>();
  }

  public BattleShipBoard(int w, int h, T missInfo) {
    this(w, h, new InBoundsRuleChecker<T>(new NoCollisionRuleChecker<T>(null)), missInfo);

  }

  @Override
  public String tryAddShip(Ship<T> toAdd) {
    String check = placementChecker.checkPlacement(toAdd, this);
    if (check != null) {
      return check;
    }
    myShips.add(toAdd);
    return null;
  }

  @Override
  public T whatIsAtForSelf(Coordinate where) {
    return whatIsAt(where, true);
  }

  @Override
  public T whatIsAtForEnemy(Coordinate where) {
    for (HashMap.Entry<Coordinate, T> strike : strikes.entrySet()) {
      if (where.equals(strike.getKey())) {
        return strike.getValue();
      }
    }
    for (Coordinate c : enemyMisses) {
      if (where.equals(c)) {
        return missInfo;
      }
    }

    return whatIsAt(where, false);
  }

  /*
 * Returns content of a square on the board.
 */
  protected T whatIsAt(Coordinate where, boolean isSelf) {
    for (Ship<T> s : myShips) {
      if (s.occupiesCoordinates(where)) {
        return s.displayInfoAt(where, isSelf);
      }

      /**
       * else if (!isSelf && enemyMisses.contains(where)) { return missInfo; }
       */
    }
    return null;
  }

  /**
   * Sonar scan: returns number of squares occupied by each ship present around a
   * coordinate C in the following pattern: * *** ***** ***C*** ***** *** *
   * 
   * @param where is the center coordinate C around which to find ships
   * @returns a hashmap of ship name and the number of squares occupied by it.
   * @throws IllegalArgumentException if C is not part of the battleship
   */
  public HashMap<String, Integer> sonar(int range, Coordinate where) throws IllegalArgumentException {
    if ((where == null) || (where.getRow() > height - 1) || (where.getColumn() > width - 1)) {
      throw new IllegalArgumentException("Invalid coordinate for sonar scan. Coordinate " + where.toString()
          + " is not part of the battleship board.");
    }

    // create coordinates in the range of sonar
    ArrayList<Coordinate> sonarCoords = new ArrayList<Coordinate>();
    int columnRange;
    for (int row = -range; row <= range; row++) {
      columnRange = range - Math.abs(row);
      for (int col = Math.min(columnRange, -columnRange); col <= Math.max(columnRange, -columnRange); col++) {
        sonarCoords.add(new Coordinate(where.getRow() + row, where.getColumn() + col));
      }
    }

    // count ships present at sonarCoords
    HashMap<String, Integer> shipCount = new HashMap<String, Integer>();
    for (Ship<T> ship : myShips) {
      shipCount.put(ship.getName(), 0);
    }

    int count;
    // Iterable<Coordinate> shipCoords = new Iterable<Coordinate>();
    for (Ship<T> ship : myShips) {
      for (Coordinate c : sonarCoords) {
        Iterable<Coordinate> shipCoords = ship.getCoordinates();
        for (Coordinate shipCoord : shipCoords) {
          if (c.equals(shipCoord)) {
            count = shipCount.get(ship.getName());
            shipCount.put(ship.getName(), count + 1);
          }
        }
      }
    }
    return shipCount;
  }

  /**
   * Checks whether or not the Board has the same content at T[][]
   */
  public boolean isBoardAsExpected(T[][] expected) {
    for (int row = 0; row < getWidth(); row++) {
      for (int col = 0; col < getHeight(); col++) {
        Coordinate currCoord = new Coordinate(row, col);
        if (whatIsAtForSelf(currCoord) != expected[row][col]) {
          return false;
        }
      }
    }
    return true;
  }

  /**
   * Records a hit at given coordinate c. If a ship is present at c then a hit is
   * recorded. Else, c is considered to be a missed hit.
   */
  @Override
  public Ship<T> fireAt(Coordinate c) {
    for (Ship<T> ship : myShips) {
      if (ship.occupiesCoordinates(c)) {
        ship.recordHitAt(c);
        ShipDisplayInfo<T> displayInfo = ship.getMyDisplayInfo();
        strikes.put(c, displayInfo.getInfo(c, false));
        return ship;
      }
    }
    enemyMisses.add(c);
    return null;
  }

  public HashSet<Coordinate> getMisses() {
    return enemyMisses;
  }

  /*
 * Returns data that is shown to the enemy on a missfire.
 */
  public T getMissInfo() {
    return missInfo;
  }

  /*
 * Whether or not all ships on the board have sunk.
 */
  public boolean allShipsHaveSunk() {
    for (Ship<T> ship : myShips) {
      if (!ship.isSunk()) {
        return false;
      }
    }
    return true;
  }

  @Override
  public void deleteShip(Coordinate where) {
    int index = 0;
    for (Ship<T> s : myShips) {
      if (s.occupiesCoordinates(where)) {
        myShips.remove(index);
        return;
      }
      index += 1;
    }
  }

  /**
   * public Iterable<Ship<T>> getShips() { return myShips; }
   */
  public Ship<T> getShipAtCoordinate(Coordinate where) {
    for (Ship<T> curr : myShips) {
      if (curr.occupiesCoordinates(where)) {
        return curr;
      }
    }
    return null;
  }
}
