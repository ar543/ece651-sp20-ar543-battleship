package edu.duke.ar543.battleship;

import java.util.function.Function;

/**
 * This class handles textual display of
 * a Board (i.e., converting it to a string to show
 * to the user).
 * It supports two ways to display the Board:
 * one for the player's own board, and one for the 
 * enemy's board.
 */
public class BoardTextView {
  /**
   * The Board to display
   */
  private Board<Character> toDisplay;
  
  /**
   * Constructs a BoardView, given the board it will display.
   * @throws IllegalArgumentException if the board is larger than 10x26.
   * @param toDisplay is the Board to display
   */
  public BoardTextView(Board<Character> toDisplay) {
    this.toDisplay = toDisplay;
    if (toDisplay.getWidth() > 10 || toDisplay.getHeight() > 26) {
      throw new IllegalArgumentException(
          "Board must be no larger than 10x26, but is " + toDisplay.getWidth() + "x" + toDisplay.getHeight());
    }
  }

  /**
  public void updateView(Board<Character> updatedDisplay) {
    if (updatedDisplay.getWidth() != toDisplay.getWidth() || updatedDisplay.getHeight() != toDisplay.getHeight()) {
      throw new IllegalArgumentException("Updated board does not match original board's dimensions.\n");
    }
    this.toDisplay = updatedDisplay;
  }
  */  
  
  /**
   * Draws toDisplay using text.  
   */
  protected String displayAnyBoard(Function<Coordinate, Character> getSquareFn) {
    StringBuilder out = new StringBuilder(this.makeHeader());
    char rowLetter = 'A';
    for (int row = 0; row < toDisplay.getHeight(); row++) {
      out.append(rowLetter);
      out.append(" ");  // actual space
      char curr;
      for (int col = 0; col < toDisplay.getWidth() - 1; col++) {
        try {
          curr = getSquareFn.apply(new Coordinate(row, col));
        }
        catch (NullPointerException e) {
          curr = ' ';
        }
        out.append(curr);
        out.append("|");
      }
      try {
        curr = getSquareFn.apply(new Coordinate(row, toDisplay.getWidth() - 1));
      }
      catch (NullPointerException e) {
          curr = ' ';
      }
      out.append(curr);
      out.append(" "); // actual space
      out.append(rowLetter);
      out.append("\n");
      rowLetter += 1;
    }
    out.append(this.makeHeader());
    return out.toString();
  }

  public String displayMyOwnBoard() {
    return displayAnyBoard((c) -> toDisplay.whatIsAtForSelf(c));
  }

  public String displayEnemyBoard() {
    return displayAnyBoard((c) -> toDisplay.whatIsAtForEnemy(c));
  }

  public String displayMyBoardWithEnemyNextToIt(BoardTextView enemyView, String myHeader, String enemyHeader) throws IllegalArgumentException {
    StringBuilder out = new StringBuilder();
    String myBoardText = this.displayMyOwnBoard();
    String enemyBoardText = enemyView.displayEnemyBoard();
    String[] myBoardLines = myBoardText.split("\n");
    String[] enemyBoardLines = enemyBoardText.split("\n");
    if (myBoardLines.length != enemyBoardLines.length) {
      throw new IllegalArgumentException("Error! Player and enemy boards have different heights.\n");
    }
    out.append("     "); // player's own board header starts after 5 spaces
    out.append(myHeader);
    for (int i = 0; i < 2 * toDisplay.getWidth() + 22; i++) {
      out.append(" ");
    }
    out.append(enemyHeader);
    out.append("\n");
    for (int lineNumber = 0; lineNumber < myBoardLines.length; lineNumber++) {
      out.append(myBoardLines[lineNumber]);
      for (int i = 0; i < 2 * toDisplay.getWidth() + 19; i++) {
        out.append(" ");
      }
      if (lineNumber == 0 || lineNumber == myBoardLines.length - 1) {
        out.append("  "); // two extra spaces after header of column numbers
      }
      out.append(enemyBoardLines[lineNumber]);
      out.append("\n");
    }
    return out.toString();
  }
  
  /**
   * This makes the header line, e.g. 0|1|2|3|4\n
   * 
   * @return the String that is the header line for the given board
   */
  String makeHeader() {
    StringBuilder ans = new StringBuilder("  "); // README shows two spaces at
    String sep=""; //start with nothing to separate, then switch to | to separate
    for (int i = 0; i < toDisplay.getWidth(); i++) {
      ans.append(sep);
      ans.append(i);
      sep = "|";
    }
    ans.append("\n");
    return ans.toString();
  }

}
