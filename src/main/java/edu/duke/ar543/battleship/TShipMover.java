package edu.duke.ar543.battleship;

import java.util.HashMap;

public class TShipMover<T> extends ShipMover<T> {

  public TShipMover(Board<T> b) {
    super(b);
  }

  @Override
  public boolean moveShip(BasicShip<T> ship, Placement to) throws IllegalArgumentException {
    if (to.getOrientation() != 'U' && to.getOrientation() != 'L' && to.getOrientation() != 'D'
        && to.getOrientation() != 'R') {
      throw new IllegalArgumentException(
          "Invalid orientation for the T-ship present at " + to.getCoordinate().toString());
    }
    HashMap<Integer, Boolean> oldPieceToHit = ship.getIntToHitsMap();
    HashMap<Integer, Coordinate> oldPieceToCoord = ship.getIntToPiecesMap();
    board.deleteShip(oldPieceToCoord.get(1));
    TShip<T> newShip = new TShip<T>(ship.getName(), to.getCoordinate(), to.getOrientation(), oldPieceToHit,
        ship.getMyDisplayInfo(), ship.getEnemyDisplayInfo());
    String shipAddLog = board.tryAddShip(newShip);
    if (shipAddLog != null) {
      board.tryAddShip(ship);
      throw new IllegalArgumentException("Could not move T-ship.\n" + shipAddLog);
    }
    return true;
  }
}
