package edu.duke.ar543.battleship;

public class V2ShipFactory implements AbstractShipFactory<Character> {

	@Override
	public Ship<Character> makeSubmarine(Placement where) {
    V1ShipFactory factory = new V1ShipFactory();
    return factory.makeSubmarine(where);
	}

	@Override
	public Ship<Character> makeBattleship(Placement where) {
    return new TShip<Character>("Battleship", where.getCoordinate(), where.getOrientation(), 'b', '*');
	}

	@Override
	public Ship<Character> makeCarrier(Placement where) {
    return new ZShip<Character>("Carrier", where.getCoordinate(), where.getOrientation(), 'c', '*');
	}

	@Override
	public Ship<Character> makeDestroyer(Placement where) {
    V1ShipFactory factory = new V1ShipFactory();
    return factory.makeDestroyer(where);
	}

}












