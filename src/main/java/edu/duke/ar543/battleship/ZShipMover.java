package edu.duke.ar543.battleship;
import java.util.HashMap;

public class ZShipMover<T> extends ShipMover<T> {

  public ZShipMover(Board<T> b) {
    super(b);
  }

  @Override
  public boolean moveShip(BasicShip<T> ship, Placement to) throws IllegalArgumentException {
    if (to.getOrientation() != 'U' && to.getOrientation() != 'L' && to.getOrientation() != 'D'
        && to.getOrientation() != 'R') {
      throw new IllegalArgumentException(
          "Invalid orientation for the Z-ship present at " + to.getCoordinate().toString());
    }
    HashMap<Integer, Boolean> oldPieceToHit = ship.getIntToHitsMap();
    HashMap<Integer, Coordinate> oldPieceToCoord = ship.getIntToPiecesMap();
    board.deleteShip(oldPieceToCoord.get(1));
    ZShip<T> newShip = new ZShip<T>(ship.getName(), to.getCoordinate(), to.getOrientation(), oldPieceToHit,
        ship.getMyDisplayInfo(), ship.getEnemyDisplayInfo());
    String shipAddLog = board.tryAddShip(newShip);
    if (shipAddLog != null) {
      board.tryAddShip(ship);
      throw new IllegalArgumentException("Could not move Z-ship.\n" + shipAddLog);
    }
    return true;
  }
}













