package edu.duke.ar543.battleship;

import java.util.Iterator;

public class InBoundsRuleChecker<T> extends PlacementRuleChecker<T> {
  public InBoundsRuleChecker(PlacementRuleChecker<T> next) {
    super(next);
  }

  /**
   * Checks whether or not ship is being placed inside the board.
   * @param theShip is the ship whose placement is checked.
   * @param theBoard is the board on which ship is being placed.
   * @return true if ship is placed within the bounds of the board.
   */
  @Override
  protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
    Iterable<Coordinate> shipCoords = theShip.getCoordinates();
    Iterator<Coordinate> coordIt = shipCoords.iterator();
    while (coordIt.hasNext()) {
      Coordinate c = coordIt.next();
      if (c.getRow() < 0) {
        return "That placement is invalid: the ship goes off the top of the board.\n";
      }
      else if (c.getRow() >= theBoard.getHeight()) {
        return "That placement is invalid: the ship goes off the bottom of the board.\n";
      }
      else if (c.getColumn() < 0) {
        return "That placement is invalid: the ship goes off the left of the board.\n";
      }

      else if (c.getColumn() >= theBoard.getWidth()) {
        return "That placement is invalid: the ship goes off the right of the board.\n";
      }
    }
    return null;
  }
}









