package edu.duke.ar543.battleship;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.function.Function;

/* Represents a player playing battleship on the terminal.
 *
 */
public class TextPlayer {
  private Board<Character> theBoard;
  private BoardTextView view;
  private final BufferedReader inputReader;
  private final PrintStream out;
  private final AbstractShipFactory<Character> shipFactory;
  private final String name;
  private final ArrayList<String> shipsToPlace;
  private final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;
  private int numMovesLeft;
  private int numSonarLeft;

  public TextPlayer(String name, Board<Character> b, BufferedReader inputReader, PrintStream out,
      AbstractShipFactory<Character> factory, int moves, int sonar) {
    this.theBoard = b;
    this.view = new BoardTextView(b);
    ;
    this.inputReader = inputReader;
    this.out = out;
    this.shipFactory = factory;
    this.name = name;
    shipsToPlace = new ArrayList<String>();
    shipCreationFns = new HashMap<String, Function<Placement, Ship<Character>>>();
    setupShipCreationMap();
    setupShipCreationList();
    numMovesLeft = moves;
    numSonarLeft = sonar;
  }

  /*
   * Sets up a mapping of ship types to a function that creates the ship.
   */
  protected void setupShipCreationMap() {
    shipCreationFns.put("Submarine", (p) -> shipFactory.makeSubmarine(p));
    shipCreationFns.put("Destroyer", (p) -> shipFactory.makeDestroyer(p));
    shipCreationFns.put("Battleship", (p) -> shipFactory.makeBattleship(p));
    shipCreationFns.put("Carrier", (p) -> shipFactory.makeCarrier(p));
  }

  /*
   * Sets up a list of the ships required to play the game.
   */
  protected void setupShipCreationList() {
    shipsToPlace.addAll(Collections.nCopies(2, "Submarine"));
    shipsToPlace.addAll(Collections.nCopies(3, "Destroyer"));
    shipsToPlace.addAll(Collections.nCopies(3, "Battleship"));
    shipsToPlace.addAll(Collections.nCopies(2, "Carrier"));
  }

  /*
  * Prompts the player for a ship placement, reads the placement and returns it.
  */
  public Placement readPlacement(String prompt) throws IOException {
    out.println(prompt);
    String s = inputReader.readLine();
    return new Placement(s);
  }

  /*
 * Prompts the players for a coordinate, reads and returns the coordinate.
 */
  public Coordinate readCoordinate(String prompt) throws IOException, IllegalArgumentException {
    out.println(prompt);
    String s = inputReader.readLine();
    return new Coordinate(s);
  }

  /*
 * Asks the player to place a ship on battleship board.
 */
  public void doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) throws IOException {
    Placement p = readPlacement("Player " + name + " where do you want to place a " + shipName + "?");
    Ship<Character> s = createFn.apply(p);
    String shipAddLogMessage = theBoard.tryAddShip(s);
    if (shipAddLogMessage != null) {
      throw new IllegalArgumentException(shipAddLogMessage);
    }
    // view.updateView(theBoard);
    out.print(view.displayMyOwnBoard());
  }

  /*
 * Returns the initial prompt that a players gets when they place their ships.
 */
  private String getPlacementPrompt(String playerName, int numSubmarines, int numDestroyers, int numBattleships,
      int numCarriers) {
    StringBuilder prompt = new StringBuilder();
    StringBuilder header = new StringBuilder();
    int headerLength = 80;
    for (int i = 0; i < headerLength; i++) {
      header.append("-");
    }
    prompt.append(header.toString() + "\n");
    prompt.append("Player ");
    prompt.append(playerName);
    prompt.append(": you are going to place the following ships " + "(which are all\nrectangular). For each ship, type "
        + "the coordinate of the upper left\nside of the ship, "
        + "followed by either H (for horizontal) or V (for\nvertical). "
        + "For example M4H would place a ship horizontally starting\nat " + "M4 and going to the right. You have\n\n");
    prompt.append(numSubmarines);
    prompt.append(" \"Submarines\" ships that are 1x2\n");
    prompt.append(numDestroyers);
    prompt.append(" \"Destroyers\" ships that are 1x3\n");
    prompt.append(numBattleships);
    prompt.append(" \"Battleships\" ships that are 1x4\n");
    prompt.append(numCarriers);
    prompt.append(" \"Carriers\" ships that are 1x6\n");
    prompt.append(header.toString() + "\n");
    return prompt.toString();
  }

  /*
 * Asks the player to place all the ships on the board
 */
  public void doPlacementPhase() throws IOException {
    boolean shipPlaced = false;
    out.print(view.displayMyOwnBoard());
    int numSubmarines = 2;
    int numDestroyers = 3;
    int numBattleships = 3;
    int numCarriers = 2;
    int shipsLeftForPlacement = numSubmarines + numDestroyers + numBattleships + numCarriers;
    out.print(getPlacementPrompt(name, numSubmarines, numDestroyers, numBattleships, numCarriers));
    for (String shipName : shipsToPlace) {
      while (!shipPlaced) {
        try {
          doOnePlacement(shipName, shipCreationFns.get(shipName));
          shipPlaced = true;
          shipsLeftForPlacement -= 1;
        } catch (IllegalArgumentException iae) {
          out.print(iae.getMessage());
        } catch (IOException ioe) {
          out.print(ioe.getMessage());
        }
        // catch (EOFException eofe) {
        // out.print("Exiting...\n");
        // System.exit(0);
        // }
      }
      shipPlaced = false;
    }
    if (shipsLeftForPlacement != 0) {
      throw new IOException("Not all ships placed.\n");
    }
  }

  /*
 * Returns player's own view of their board.
 */  
  public BoardTextView getView() {
    return view;
  }

  /*
 * Returns players board.
 */
  public Board<Character> getBoard() {
    return theBoard;
  }

  public void setBoard(Board<Character> b) {
    theBoard = b;
  }

  /*
  * Returns a string that prompts player to choose a move to make such
  * as (M)ove, (F)ire, and (S)onar.  
  */
  private String getActionPrompt() {
    StringBuilder prompt = new StringBuilder();
    StringBuilder header = new StringBuilder();
    int headerLength = 80;
    for (int i = 0; i < headerLength; i++) {
      header.append("-");
    }
    prompt.append(header.toString() + "\n");
    prompt.append("Possible action for ");
    prompt.append(this.name);
    prompt.append(":\n\n");
    prompt.append("F Fire at a square\n");
    prompt.append("M Move a ship to another square (" + numMovesLeft + " remaining)\n");
    prompt.append("S Sonar scan (" + numSonarLeft + " remaining)\n\n");
    prompt.append(name + ", what would you like to do?\n");
    prompt.append(header.toString() + "\n");
    return prompt.toString();
  }

  /*
 * Returns choice selected by user which is either (M)ove, (F)ire or (S)onar.
 */
  public char readChoice() throws IOException, IllegalArgumentException {
    char choice;
    out.println(getActionPrompt());
    String stringChoice;
    try {
      stringChoice = inputReader.readLine();
      stringChoice = stringChoice.toUpperCase();
      char[] choiceArray = stringChoice.toCharArray();
      if (choiceArray.length != 1) {
        throw new IllegalArgumentException("Invalid option select! Please select one of the options shown.\n");
      }
      choice = choiceArray[0];
      if (!(choice == 'F' || choice == 'M' || choice == 'S')) {
        throw new IllegalArgumentException("Invalid option select! Please select one of the options shown.\n");
      }
      return choice;
    } catch (EOFException ioe) {
      System.exit(0);
    }
    return 'a';
  }

  /*
 * Checks whether or not a coordinate is within the bounds of the board of the player.
 */
  private boolean withinBoard(Coordinate c, Board<Character> board) {
    if (c.getRow() >= board.getHeight() || c.getColumn() >= board.getWidth()) {
      return false;
    }
    return true;
  }

  private void printSonarResult(HashMap<String, Integer> shipCount) {
    StringBuilder result = new StringBuilder();
    StringBuilder header = new StringBuilder();
    int headerLength = 80;
    for (int i = 0; i < headerLength; i++) {
      header.append("-");
    }
    result.append(header.toString() + "\n");
    result.append("Submarines occupy " + shipCount.get("Submarine") + " squares\n");
    result.append("Destroyers occupy " + shipCount.get("Destroyer") + " squares\n");
    result.append("Battleships occupy " + shipCount.get("Battleship") + " squares\n");
    result.append("Carriers occupy " + shipCount.get("Carrier") + " squares\n");
    result.append(header.toString());
    out.println(result.toString());
  }

  /*
  * Moves player's ship from one place in the boarrd to other keeping
  * the damage consistent.  
  */
  private void  moveOwnShip() throws IOException, IllegalArgumentException {
    if (numMovesLeft <= 0) {
      throw new IllegalArgumentException("All moves used. Choose a different option.\n");
    }
    Coordinate moveFrom = readCoordinate("Enter source coordinate:\n");
    if (!withinBoard(moveFrom, theBoard)) {
      throw new IllegalArgumentException("Source coordinate out of bounds from your board.");
    }

    Placement moveTo = readPlacement("Enter destination placement:\n");
    if (!withinBoard(moveTo.getCoordinate(), theBoard)) {
      throw new IllegalArgumentException("Destination coordinate out of bounds from your board.");
    }

    Ship<Character> shipToMove = theBoard.getShipAtCoordinate(moveFrom);
    String shipName = shipToMove.getName();
    if (shipName == "Submarine" || shipName == "Destroyer") {
      RectangleShipMover<Character> mover = new RectangleShipMover<Character>(theBoard);
      mover.moveShip((BasicShip<Character>) shipToMove, moveTo);
    } else if (shipName == "Battleship") {
      TShipMover<Character> mover = new TShipMover<Character>(theBoard);
      mover.moveShip((BasicShip<Character>) shipToMove, moveTo);
    } else if (shipName == "Carrier") {
      ZShipMover<Character> mover = new ZShipMover<Character>(theBoard);
      mover.moveShip((BasicShip<Character>) shipToMove, moveTo);
    } else {
      throw new IllegalArgumentException("Unknown ship type for movement.");
    }
    numMovesLeft -= 1;
  }

  /*
 * Fires at a square on the enemy's board selected by the player.
 */
  private void fireAtEnemy(Board<Character> enemyBoard) throws IllegalArgumentException, IOException {
    Coordinate hitAt = readCoordinate("Where do you want to hit your enemy? Enter a coordinate:\n");
    if (!withinBoard(hitAt, enemyBoard)) {
      throw new IllegalArgumentException("Coordinate out of bounds of the enemy board.");
    }
    Ship<Character> shipHit = enemyBoard.fireAt(hitAt);
    if (shipHit != null) {
      out.println("You hit a " + shipHit.getName() + "!");
    } else {
      out.println("You missed!");
    }
  }

  /*
 * Scans for ships and number of squares occupied by the ships in the vicinity of a given square.
 */
  private void doSonarScan(Board<Character> enemyBoard) throws IllegalArgumentException, IOException {
    if (numSonarLeft <= 0) {
      throw new IllegalArgumentException("All sonar scans used. Choose a different option.");
    }
    Coordinate sonarAt = readCoordinate("Enter a coordinate for sonar scan:\n");
    if (!withinBoard(sonarAt, enemyBoard)) {
      throw new IllegalArgumentException("Coordinate out of bounds of the enemy board.");
    }
    int sonarRadius = 3;
    HashMap<String, Integer> shipCount = enemyBoard.sonar(sonarRadius, sonarAt);
    printSonarResult(shipCount);
    numSonarLeft -= 1;
  }

  /*
 * Executes the choice selected by player by either moving one's own ship, sonar scanning or firing at enemy's board.
 */
  public boolean playChoice(char choice, Board<Character> enemyBoard) throws IOException, IllegalArgumentException {
    if (choice == 'M') {
      moveOwnShip();
    } else if (choice == 'F') {
      fireAtEnemy(enemyBoard);
    } else if (choice == 'S') {
      doSonarScan(enemyBoard);
    }
    return true;
  }

  /*
 * Plays a single turn for a player.
 */
  public void playOneTurn(TextPlayer enemy, String myHeader, String enemyHeader) throws IOException {
    Board<Character> enemyBoard = enemy.getBoard();
    BoardTextView enemyView = enemy.getView();
    out.println(view.displayMyBoardWithEnemyNextToIt(enemyView, myHeader, enemyHeader));
    boolean turnPlayed = false;
    while (!turnPlayed) {
      try {
        char choice = readChoice();
        turnPlayed = playChoice(choice, enemyBoard);
      } catch (IllegalArgumentException iae) {
        out.println(iae.getMessage() + "\n");
      }
    }
  }

  /*
 * Return player name.
 */
  public String getName() {
    return name;
  }

  /*
 * Prints a message after player wins.
 */
  public void declareWinner() {
    out.println("Congratulations " + name + ". You have sunk all of your opponent's ships.");
  }

  /*
  * Determines whether or not all ships of a player have sunk.
  * Player loses if all ships have sunk.  
  */
  public boolean hasLost() {
    return theBoard.allShipsHaveSunk();
  }
}
