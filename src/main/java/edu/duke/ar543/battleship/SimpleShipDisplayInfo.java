package edu.duke.ar543.battleship;

public class SimpleShipDisplayInfo<T> implements ShipDisplayInfo<T> {
  private T myData;
  private T onHit;

  public SimpleShipDisplayInfo(T d, T h) {
    this.myData = d;
    this.onHit = h;
  }
  
	@Override
	public T getInfo(Coordinate where, boolean hit) {
    if (hit) {
      return onHit;
    }
    return myData;
	}  
}
