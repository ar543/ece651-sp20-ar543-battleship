package edu.duke.ar543.battleship;

import java.util.HashMap;

/*
 * Moves a rectangle ship on a given BattleShipBoard.
 */
public class RectangleShipMover<T> extends ShipMover<T> {

  public RectangleShipMover(Board<T> b) {
    super(b);
  }
  
	@Override
	public boolean moveShip(BasicShip<T> ship, Placement to) throws IllegalArgumentException {
    if (to.getOrientation() != 'V' && to.getOrientation() != 'H') {
      throw new IllegalArgumentException("Invalid orientation for the ship present at " + to.getCoordinate().toString());
    }
    
    int newShipWidth;
    int newShipHeight;
    if (to.getOrientation() == 'V') {
      newShipWidth = Math.min(ship.getWidth(), ship.getHeight());
      newShipHeight = Math.max(ship.getWidth(), ship.getHeight());;
    }
    else {
      newShipWidth = Math.max(ship.getWidth(), ship.getHeight());
      newShipHeight = Math.min(ship.getWidth(), ship.getHeight());;
    }
    HashMap<Integer, Boolean> pieceToHit = ship.getIntToHitsMap();
    RectangleShip<T> newShip = new RectangleShip<T>(ship.getName(),
                                                    to.getCoordinate(),
                                                    newShipWidth,
                                                    newShipHeight,
                                                    pieceToHit,
                                                    ship.getMyDisplayInfo(),
                                                    ship.getEnemyDisplayInfo());
    // delete ship from old location
    board.deleteShip(ship.getStartingCoordinate());

    // add ship at new location
    String shipAddLog = board.tryAddShip(newShip);
    if (shipAddLog == null) {
      //board.deleteShip(ship.getStartingCoordinate());
    }
    else {
      board.tryAddShip(ship);
      throw new IllegalArgumentException("Could not move ship.\n" + shipAddLog);
    }
    return true;
	}
}












