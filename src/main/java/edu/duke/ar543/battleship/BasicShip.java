package edu.duke.ar543.battleship;

import java.util.HashMap;

public abstract class BasicShip<T> implements Ship<T> {
  protected final String name;
  protected HashMap<Integer, Boolean> pieceToHit; // piece id to hit map
  protected HashMap<Integer, Coordinate> pieceToCoord;  // piece id to coordinate map
  protected ShipDisplayInfo<T> myDisplayInfo;
  protected ShipDisplayInfo<T> enemyDisplayInfo;
  protected int width;
  protected int height;

  public BasicShip(String name,
                   HashMap<Integer, Coordinate> where,
                   ShipDisplayInfo<T> myDisplayInfo,
                   ShipDisplayInfo<T> enemyDisplayInfo) {
    pieceToHit = new HashMap<Integer, Boolean>();
    pieceToCoord = where;
    int id = 1; // start from 1 and increment after each ship is added
    for (HashMap.Entry<Integer, Coordinate> entry: pieceToCoord.entrySet()) {
      pieceToHit.put(entry.getKey(), false);
    }
    this.myDisplayInfo = myDisplayInfo;
    this.enemyDisplayInfo = enemyDisplayInfo;
    this.name = name;
  }

  public BasicShip(String name,
                   HashMap<Integer, Coordinate> where,
                   HashMap<Integer, Boolean> hits,
                   ShipDisplayInfo<T> myDisplayInfo,
                   ShipDisplayInfo<T> enemyDisplayInfo) {
    pieceToHit = hits;
    pieceToCoord = where;
    this.myDisplayInfo = myDisplayInfo;
    this.enemyDisplayInfo = enemyDisplayInfo;
    this.name = name;
  }

  protected void checkCoordinateInThisShip(Coordinate c) throws IllegalArgumentException {
    if (!occupiesCoordinates(c)) {
      throw new IllegalArgumentException("Coordinate " + c.toString() + " is not part of the ship.\n");
    }
  }
  
	@Override
  public boolean occupiesCoordinates(Coordinate where) {
    for (Coordinate c : pieceToCoord.values()) {
      if (c.equals(where)) {
        return true;
      }
    }
    return false;
    //return myPieces.get(where) != null;
  }

	@Override
	public boolean isSunk() {
    for (HashMap.Entry<Integer, Boolean> entry : pieceToHit.entrySet()) {
      if (entry.getValue() == false) {
        return false;
      }
    }
    return true;
	}
  
	@Override
	public void recordHitAt(Coordinate where) throws IllegalArgumentException {
    checkCoordinateInThisShip(where);
    for (HashMap.Entry<Integer, Coordinate> entry: pieceToCoord.entrySet()) {
      if (where.equals(entry.getValue())) {
        pieceToHit.put(entry.getKey(), true);
      }
    }
    //myPieces.put(where, true);
	}

	@Override
	public boolean wasHitAt(Coordinate where) throws IllegalArgumentException {
    checkCoordinateInThisShip(where);
    boolean wasHit = false;
    for (HashMap.Entry<Integer, Coordinate> entry: pieceToCoord.entrySet()) {
      if (where.equals(entry.getValue())) {
        wasHit = pieceToHit.get(entry.getKey());
      }
    }
    return wasHit;
    //return myPieces.get(where);
	}

	@Override
	public T displayInfoAt(Coordinate where, boolean myShip) throws IllegalArgumentException {
    checkCoordinateInThisShip(where);
    if (myShip) {
      return myDisplayInfo.getInfo(where, wasHitAt(where));
    }
    return enemyDisplayInfo.getInfo(where, wasHitAt(where));
	}

  @Override
  public Iterable<Coordinate> getCoordinates() {
    return pieceToCoord.values();
  }

  public HashMap<Integer, Coordinate> getIntToPiecesMap() {
    return pieceToCoord;
  }

  public HashMap<Integer, Boolean> getIntToHitsMap() {
    return pieceToHit;
  }
  
  @Override
  public Coordinate getStartingCoordinate() {
    return pieceToCoord.get(1);
  }

  public ShipDisplayInfo<T> getMyDisplayInfo() {
    return this.myDisplayInfo;
  }

  public ShipDisplayInfo<T> getEnemyDisplayInfo() {
    return this.enemyDisplayInfo;
  }

  public int getWidth() {
    return width;
  }

  public int getHeight() {
    return height;
  }

  public String getName() {
    return name;
  }
}













