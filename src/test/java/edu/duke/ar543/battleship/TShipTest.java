package edu.duke.ar543.battleship;
import java.util.HashMap;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TShipTest {
  @Test
  public void test_constructor() {
    HashMap<Integer, Coordinate> expectedUp = new HashMap<Integer, Coordinate>();
    expectedUp.put(1, new Coordinate(1,3));
    expectedUp.put(2, new Coordinate(2,2));
    expectedUp.put(3, new Coordinate(2,3));
    expectedUp.put(4, new Coordinate(2,4));
    TShip<Character> up = new TShip<Character>("Battleship", new Coordinate(1,2), 'U', 'b', '*');
    assertEquals(expectedUp, up.getIntToPiecesMap());
    
    HashMap<Integer, Coordinate> expectedRight = new HashMap<Integer, Coordinate>();
    expectedRight.put(1, new Coordinate(4,3));
    expectedRight.put(2, new Coordinate(3,2));
    expectedRight.put(3, new Coordinate(4,2));
    expectedRight.put(4, new Coordinate(5,2));
    TShip<Character> right = new TShip<Character>("Battleship", new Coordinate(3,2), 'R', 'b', '*');
    assertEquals(expectedRight, right.getIntToPiecesMap());
    
    HashMap<Integer, Coordinate> expectedDown = new HashMap<Integer, Coordinate>();
    expectedDown.put(1, new Coordinate(6,7));
    expectedDown.put(2, new Coordinate(5,8));
    expectedDown.put(3, new Coordinate(5,7));
    expectedDown.put(4, new Coordinate(5,6));
    TShip<Character> down = new TShip<Character>("Battleship", new Coordinate(5,6), 'D', 'b', '*');
    assertEquals(expectedDown, down.getIntToPiecesMap());
    
    HashMap<Integer, Coordinate> expectedLeft = new HashMap<Integer, Coordinate>();
    expectedLeft.put(1, new Coordinate(2,6));
    expectedLeft.put(2, new Coordinate(3,7));
    expectedLeft.put(3, new Coordinate(2,7));
    expectedLeft.put(4, new Coordinate(1,7));
    TShip<Character> left = new TShip<Character>("Battleship", new Coordinate(1,6), 'L', 'b', '*');
    assertEquals(expectedLeft, left.getIntToPiecesMap());
    assertThrows(IllegalArgumentException.class,
        () -> new TShip<Character>("Battleship", new Coordinate(1, 6), 'A', 'b', '*'));
  }

  @Test
  public void test_get_starting_coordinate() {
    Coordinate c = new Coordinate(1,2);
    TShip<Character> up = new TShip<Character>("Battleship", c, 'U', 'b', '*');
    TShip<Character> right = new TShip<Character>("Battleship", c, 'R', 'b', '*');
    TShip<Character> down = new TShip<Character>("Battleship", c, 'D', 'b', '*');
    TShip<Character> left = new TShip<Character>("Battleship", c, 'L', 'b', '*');

    assertEquals(c,up.getStartingCoordinate());
    assertEquals(c,right.getStartingCoordinate());
    assertEquals(c,down.getStartingCoordinate());
    assertEquals(c,left.getStartingCoordinate());
  }

}











