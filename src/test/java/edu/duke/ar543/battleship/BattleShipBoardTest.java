
package edu.duke.ar543.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.HashSet;
import java.util.HashMap;

import org.junit.jupiter.api.Test;

public class BattleShipBoardTest {
  @Test
  public void test_width_and_height() {
    BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
    assertEquals(10, b1.getWidth());
    assertEquals(20, b1.getHeight());
  }

  @Test
  public void test_invalid_dimensions() {
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(10, 0, 'X'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(0, 20, 'X'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(10, -5, 'X'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(-8, 20, 'X'));
  }

  @Test
  public void test_if_returns_right_coordinates() {
    int width = 6;
    int height = 8;
    BattleShipBoard<Character> b = new BattleShipBoard<Character>(width, height, 'X');
    Character[][] expected = new Character[width][height];
    assertEquals(true, b.isBoardAsExpected(expected));

    Coordinate c1 = new Coordinate(0, 0);
    Coordinate c2 = new Coordinate(1, 2);
    RectangleShip<Character> s1 = new RectangleShip<Character>(c1, 's', '*');
    RectangleShip<Character> s2 = new RectangleShip<Character>(c2, 's', '*');
    b.tryAddShip(s1);
    b.tryAddShip(s2);
    expected[0][0] = 's';
    expected[1][2] = 's';
    assertEquals(true, b.isBoardAsExpected(expected));

    expected[0][1] = 's';
    assertNotEquals(true, b.isBoardAsExpected(expected));

    assertEquals("testship", s1.getName());
  }

  @Test
  public void test_fire_at() {
    BattleShipBoard<Character> b = new BattleShipBoard<Character>(10, 20, 'X');
    V1ShipFactory factory = new V1ShipFactory();
    Ship<Character> submarine = factory.makeSubmarine(new Placement(new Coordinate(0, 0), 'v'));
    Ship<Character> destroyer = factory.makeDestroyer(new Placement(new Coordinate(2, 4), 'v'));
    Ship<Character> battleship = factory.makeBattleship(new Placement(new Coordinate(7, 3), 'v'));
    Ship<Character> carrier = factory.makeCarrier(new Placement(new Coordinate(3, 5), 'v'));
    b.tryAddShip(submarine);
    b.tryAddShip(destroyer);
    b.tryAddShip(battleship);
    b.tryAddShip(carrier);

    assertSame(b.fireAt(new Coordinate(0, 0)), submarine);
    assertEquals(false, submarine.isSunk());
    assertSame(b.fireAt(new Coordinate(1, 0)), submarine);
    assertEquals(true, submarine.isSunk());

    assertSame(b.fireAt(new Coordinate(2, 4)), destroyer);
    assertEquals(false, destroyer.isSunk());
    assertSame(b.fireAt(new Coordinate(3, 4)), destroyer);
    assertEquals(false, destroyer.isSunk());
    assertSame(b.fireAt(new Coordinate(4, 4)), destroyer);
    assertEquals(true, destroyer.isSunk());

    assertSame(b.fireAt(new Coordinate(7, 3)), battleship);
    assertEquals(false, battleship.isSunk());
    assertSame(b.fireAt(new Coordinate(8, 3)), battleship);
    assertEquals(false, battleship.isSunk());
    assertSame(b.fireAt(new Coordinate(9, 3)), battleship);
    assertEquals(false, battleship.isSunk());
    assertSame(b.fireAt(new Coordinate(10, 3)), battleship);
    assertEquals(true, battleship.isSunk());

    assertSame(b.fireAt(new Coordinate(3, 5)), carrier);
    assertEquals(false, carrier.isSunk());
    assertSame(b.fireAt(new Coordinate(4, 5)), carrier);
    assertEquals(false, carrier.isSunk());
    assertSame(b.fireAt(new Coordinate(5, 5)), carrier);
    assertEquals(false, carrier.isSunk());
    assertSame(b.fireAt(new Coordinate(6, 5)), carrier);
    assertEquals(false, carrier.isSunk());
    assertSame(b.fireAt(new Coordinate(7, 5)), carrier);
    assertEquals(false, carrier.isSunk());
    assertEquals(false, b.allShipsHaveSunk());
    assertSame(b.fireAt(new Coordinate(8, 5)), carrier);
    assertEquals(true, carrier.isSunk());

    assertEquals(true, b.allShipsHaveSunk());

    assertEquals(b.fireAt(new Coordinate(4, 7)), null);
    assertEquals(b.fireAt(new Coordinate(10, 20)), null);
    HashSet<Coordinate> misses = b.getMisses();
    assertEquals(true, misses.contains(new Coordinate(4, 7)));
    assertEquals(true, misses.contains(new Coordinate(10, 20)));
  }

  @Test
  public void test_what_is_at() {
    BattleShipBoard<Character> b = new BattleShipBoard<Character>(10, 20, 'X');
    V1ShipFactory factory = new V1ShipFactory();
    Ship<Character> submarine = factory.makeSubmarine(new Placement(new Coordinate(0, 0), 'v'));
    Ship<Character> destroyer = factory.makeDestroyer(new Placement(new Coordinate(2, 4), 'v'));
    Ship<Character> battleship = factory.makeBattleship(new Placement(new Coordinate(7, 3), 'v'));
    Ship<Character> carrier = factory.makeCarrier(new Placement(new Coordinate(3, 5), 'v'));
    b.tryAddShip(submarine);
    b.tryAddShip(destroyer);
    b.tryAddShip(battleship);
    b.tryAddShip(carrier);

    Coordinate fire1 = new Coordinate(0, 0);
    Coordinate fire2 = new Coordinate(3, 4);
    Coordinate fire3 = new Coordinate(10, 3);
    Coordinate fire4 = new Coordinate(7, 5);
    b.fireAt(fire1);
    b.fireAt(fire2);
    b.fireAt(fire3);
    b.fireAt(fire4);

    assertEquals('*', b.whatIsAtForSelf(fire1));
    assertEquals('s', b.whatIsAtForEnemy(fire1));
    assertEquals('*', b.whatIsAtForSelf(fire2));
    assertEquals('d', b.whatIsAtForEnemy(fire2));
    assertEquals('*', b.whatIsAtForSelf(fire3));
    assertEquals('b', b.whatIsAtForEnemy(fire3));
    assertEquals('*', b.whatIsAtForSelf(fire4));
    assertEquals('c', b.whatIsAtForEnemy(fire4));

    Coordinate fire5 = new Coordinate(3,5);
    b.fireAt(fire5);

    // move carrier
    RectangleShipMover<Character> mover = new RectangleShipMover<Character>(b);
    mover.moveShip((BasicShip<Character>)carrier, new Placement("d1v"));
    assertEquals('c', b.whatIsAtForEnemy(fire5));
  }

  @Test
  public void test_delete_ship() {
    BattleShipBoard<Character> b = new BattleShipBoard<Character>(10, 20, 'X');
    V1ShipFactory factory = new V1ShipFactory();
    Ship<Character> submarine = factory.makeSubmarine(new Placement(new Coordinate(0,0), 'v'));
    Ship<Character> destroyer = factory.makeDestroyer(new Placement(new Coordinate(2,4), 'v'));
    Ship<Character> battleship = factory.makeBattleship(new Placement(new Coordinate( 7,3), 'h'));
    Ship<Character> carrier = factory.makeCarrier(new Placement(new Coordinate(15,2),  'h'));
    b.tryAddShip(submarine); 
    b.tryAddShip(destroyer); 
    b.tryAddShip(battleship);
    b.tryAddShip(carrier);

    Coordinate c1 = new Coordinate(0,0);
    Coordinate c2 = new Coordinate(1,0);
    Coordinate c3 = new Coordinate(4, 4);
    Coordinate c4 = new Coordinate(15 ,6);
    b.deleteShip(c1); 
    assertEquals(null, b.whatIsAt(c1,  true));
    assertEquals(null, b.whatIsAt(c2, true));
    b.deleteShip(c3);
    assertEquals(null, b.whatIsAt(new Coordinate(2,4), true));
    assertEquals(null, b.whatIsAt(new Coordinate(3,4), true));
    assertEquals(null, b.whatIsAt(c3, true)); 
    b.deleteShip(c3); 
    assertEquals(null, b.whatIsAt(c3, true));
    b.deleteShip(c4);
    assertEquals(null, b.whatIsAt(new Coordinate(15,2), true));
    assertEquals(null, b.whatIsAt(new Coordinate(15,3), true));
    assertEquals(null, b.whatIsAt(new Coordinate(15, 4), true));
    assertEquals(null, b.whatIsAt(new Coordinate(15, 5), true));
    assertEquals(null, b.whatIsAt(c4, true)); 
    assertEquals(null, b.whatIsAt(new Coordinate(15, 7), true));
  }
 
  @Test
  public void test_sonar() {
    BattleShipBoard<Character> b = new BattleShipBoard<Character>(10, 20, 'X');
    V2ShipFactory factory = new V2ShipFactory();
    Ship<Character> submarine = factory.makeSubmarine(new Placement(new Coordinate(0,0), 'v'));
    Ship<Character> destroyer = factory.makeDestroyer(new Placement(new Coordinate(2,4), 'v'));
    Ship<Character> battleship = factory.makeBattleship(new Placement(new Coordinate( 7,3), 'u'));
    Ship<Character> carrier = factory.makeCarrier(new Placement(new Coordinate(10,2),  'l'));
    b.tryAddShip(submarine); 
    b.tryAddShip(destroyer); 
    b.tryAddShip(battleship);
    b.tryAddShip(carrier);

    BoardTextView view = new BoardTextView(b);
    //assertEquals("", view.displayMyOwnBoard());

    Coordinate center = new Coordinate("g3");
    HashMap<String, Integer> count = b.sonar(3, center);
    HashMap<String, Integer> expected = new HashMap<String, Integer>();
    expected.put("Submarine", 0);
    expected.put("Destroyer", 1);
    expected.put("Battleship", 3);
    expected.put("Carrier", 0);
    assertEquals(expected, count);

    assertThrows(IllegalArgumentException.class, () -> b.sonar(3, new Coordinate(100, 3)));
  }

  @Test
  public void test_get_coordinates() {
    BattleShipBoard<Character> b = new BattleShipBoard<Character>(10, 20, 'X');
    V2ShipFactory factory = new V2ShipFactory();
    Ship<Character> submarine = factory.makeSubmarine(new Placement(new Coordinate(0,0), 'v'));
    Ship<Character> destroyer = factory.makeDestroyer(new Placement(new Coordinate(2,4), 'v'));
    Ship<Character> battleship = factory.makeBattleship(new Placement(new Coordinate( 7,3), 'u'));
    Ship<Character> carrier = factory.makeCarrier(new Placement(new Coordinate(10,2),  'l'));
    b.tryAddShip(submarine); 
    b.tryAddShip(destroyer); 
    b.tryAddShip(battleship);
    b.tryAddShip(carrier);

    assertEquals(submarine, b.getShipAtCoordinate(new Coordinate(0,0)));
    assertEquals(null, b.getShipAtCoordinate(new Coordinate(1,3)));
  } 
 
  @Test
  public void test_missinfo() {
    BattleShipBoard<Character> b = new BattleShipBoard<Character>(10, 20, 'X');
    assertEquals('X', b.getMissInfo());
  }
}


