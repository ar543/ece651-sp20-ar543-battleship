package edu.duke.ar543.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class ZShipMoverTest {
  @Test
  public void test_move_ship() {
    BattleShipBoard<Character> b = new BattleShipBoard<Character>(10, 15, 'X');
    V2ShipFactory factory = new V2ShipFactory();
    Ship<Character> carrier = factory.makeCarrier(new Placement(new Coordinate(3,4), 'd'));
    Ship<Character> carrier2 = factory.makeCarrier(new Placement(new Coordinate(0,7), 'u'));
    b.tryAddShip(carrier);
    b.tryAddShip(carrier2);

    BoardTextView view = new BoardTextView(b);
    //assertEquals("", view.displayMyOwnBoard());
    assertEquals('c', b.whatIsAt(new Coordinate(7,4), true));
    assertEquals('c', b.whatIsAt(new Coordinate(6,4), true));
    assertEquals('c', b.whatIsAt(new Coordinate(5,4), true));
    assertEquals('c', b.whatIsAt(new Coordinate(5,5), true));
    assertEquals('c', b.whatIsAt(new Coordinate(4,5), true));
    assertEquals('c', b.whatIsAt(new Coordinate(3,5), true));
    ZShipMover<Character> mover = new ZShipMover<Character>(b);
    mover.moveShip((BasicShip<Character>)carrier, new Placement("d2l"));
    //assertEquals("", view.displayMyOwnBoard());
    assertEquals(null, b.whatIsAt(new Coordinate(7,4), true));
    assertEquals(null, b.whatIsAt(new Coordinate(6,4), true));
    assertEquals(null, b.whatIsAt(new Coordinate(5,4), true));
    assertEquals(null, b.whatIsAt(new Coordinate(5,5), true));
    assertEquals(null, b.whatIsAt(new Coordinate(3,5), true));
    assertEquals('c', b.whatIsAt(new Coordinate(3,2), true));
    assertEquals('c', b.whatIsAt(new Coordinate(3,3), true));
    assertEquals('c', b.whatIsAt(new Coordinate(3,4), true));
    assertEquals('c', b.whatIsAt(new Coordinate(4,4), true));
    assertEquals('c', b.whatIsAt(new Coordinate(4,5), true));
    assertEquals('c', b.whatIsAt(new Coordinate(4,6), true));

    Ship<Character> submarine = factory.makeSubmarine(new Placement("A0v"));
    b.tryAddShip(submarine);
    assertThrows(IllegalArgumentException.class, () -> mover.moveShip((BasicShip<Character>)carrier2, new Placement("A0u")));
    assertThrows(IllegalArgumentException.class, () -> mover.moveShip((BasicShip<Character>)carrier2, new Placement("C3R")));
    assertThrows(IllegalArgumentException.class, () -> mover.moveShip((BasicShip<Character>)carrier2, new Placement("I0H")));
    assertThrows(IllegalArgumentException.class, () -> mover.moveShip((BasicShip<Character>)carrier2, new Placement("Z2R")));
  }

}
