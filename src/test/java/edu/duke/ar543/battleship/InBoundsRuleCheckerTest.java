package edu.duke.ar543.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class InBoundsRuleCheckerTest {
  @Test
  public void test_inbound_rule() {
    V1ShipFactory factory = new V1ShipFactory();
    InBoundsRuleChecker<Character> ibrc = new InBoundsRuleChecker<Character>(null);
    BattleShipBoard<Character> b = new BattleShipBoard<Character>(8, 10, ibrc, 'X');
    assertEquals(b.tryAddShip(factory.makeSubmarine(new Placement("A3V"))), null);
    assertEquals(b.tryAddShip(factory.makeSubmarine(new Placement("A3V"))), null);
    assertEquals(b.tryAddShip(factory.makeSubmarine(new Placement("J3V"))),
                 "That placement is invalid: the ship goes off the bottom of the board.\n");
    assertEquals(b.tryAddShip(factory.makeSubmarine(new Placement("A9V"))),
                 "That placement is invalid: the ship goes off the right of the board.\n");
    assertEquals(b.tryAddShip(factory.makeSubmarine(new Placement("I9V"))),
                 "That placement is invalid: the ship goes off the right of the board.\n");
    assertEquals(b.tryAddShip(factory.makeSubmarine(new Placement(new Coordinate(-1, 3), 'h'))),
                 "That placement is invalid: the ship goes off the top of the board.\n");
    assertEquals(b.tryAddShip(factory.makeSubmarine(new Placement(new Coordinate(3, -1), 'h'))),
                 "That placement is invalid: the ship goes off the left of the board.\n");
    assertEquals(b.tryAddShip(factory.makeSubmarine(new Placement(new Coordinate(10, 4), 'v'))),
                 "That placement is invalid: the ship goes off the bottom of the board.\n");
    assertEquals(b.tryAddShip(factory.makeSubmarine(new Placement(new Coordinate(5, 10), 'v'))),
                 "That placement is invalid: the ship goes off the right of the board.\n");
    assertEquals(b.tryAddShip(factory.makeSubmarine(new Placement(new Coordinate(2, 9), 'h'))),
                 "That placement is invalid: the ship goes off the right of the board.\n");
  }

}











