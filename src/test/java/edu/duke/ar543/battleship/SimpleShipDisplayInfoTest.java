package edu.duke.ar543.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class SimpleShipDisplayInfoTest {
  @Test
  public void test_get_info() {
    Coordinate c = new Coordinate(1,2);
    Character data = 's';
    Character hit = 'x';
    SimpleShipDisplayInfo<Character> info = new SimpleShipDisplayInfo<Character>(data, hit);
    assertEquals(info.getInfo(c, true), hit);
    assertEquals(info.getInfo(c, false), data);
  }

}










