package edu.duke.ar543.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class V1ShipFactoryTest {

  private void checkShip(Ship<Character> testShip, String expectedName,
                         char expectedLetter, Coordinate... expectedLocs) {
    assertEquals(testShip.getName(), expectedName);
    for (Coordinate c: expectedLocs) {
      assertEquals(testShip.occupiesCoordinates(c), true);
      assertEquals(expectedLetter, testShip.displayInfoAt(c, true));
    }
  }

  @Test
  public void test_submarine() {
    V1ShipFactory factory = new V1ShipFactory();
    Placement p = new Placement("A2V");
    Coordinate c1 = new Coordinate("A2");
    Coordinate c2 = new Coordinate("B2");
    Ship<Character> submarine = factory.makeSubmarine(p);
    checkShip(submarine, "Submarine", 's', c1, c2);
  }

  @Test
  public void test_destroyer() {
    V1ShipFactory factory = new V1ShipFactory();
    Placement p = new Placement("A2V");
    Coordinate c1 = new Coordinate("A2");
    Coordinate c2 = new Coordinate("B2");
    Coordinate c3 = new Coordinate("C2");
    Ship<Character> destroyer = factory.makeDestroyer(p);
    checkShip(destroyer, "Destroyer", 'd', c1, c2, c3);
    
  }

    @Test
  public void test_battleship() {
    V1ShipFactory factory = new V1ShipFactory();
    Placement p = new Placement("A2V");
    Coordinate c1 = new Coordinate("A2");
    Coordinate c2 = new Coordinate("B2");
    Coordinate c3 = new Coordinate("C2");
    Coordinate c4 = new Coordinate("D2");
    Ship<Character> battleship = factory.makeBattleship(p);
    checkShip(battleship, "Battleship", 'b', c1, c2, c3, c4);
    
  }

    @Test
  public void test_carrier() {
    V1ShipFactory factory = new V1ShipFactory();
    Placement p = new Placement("A2V");
    Coordinate c1 = new Coordinate("A2");
    Coordinate c2 = new Coordinate("B2");
    Coordinate c3 = new Coordinate("C2");
    Coordinate c4 = new Coordinate("D2");
    Coordinate c5 = new Coordinate("E2");
    Coordinate c6 = new Coordinate("F2");
    Ship<Character> carrier = factory.makeCarrier(p);
    checkShip(carrier, "Carrier", 'c', c1, c2, c3, c4, c5, c6);
    
  }
}











