package edu.duke.ar543.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

public class BoardTextViewTest {
  @Test
  public void test_display_empty_2by2() {
    Board<Character> b = new BattleShipBoard<Character>(2, 2, 'X');
    BoardTextView view = new BoardTextView(b);
    String expectedHeader = "  0|1\n";
    assertEquals(expectedHeader, view.makeHeader());
    String expected = expectedHeader + "A  |  A\n" + "B  |  B\n" + expectedHeader;
    assertEquals(expected, view.displayMyOwnBoard());
    Coordinate c = new Coordinate(0, 0);
    RectangleShip<Character> s = new RectangleShip<Character>(c, 's', '*');
    b.tryAddShip(s);

    String withShip = expectedHeader + "A s|  A\n" + "B  |  B\n" + expectedHeader;
    assertEquals(withShip, view.displayMyOwnBoard());
  }

  @Test
  public void test_invalid_board_size() {
    Board<Character> wideBoard = new BattleShipBoard<Character>(11, 20, 'X');
    Board<Character> tallBoard = new BattleShipBoard<Character>(10, 27, 'X');
    assertThrows(IllegalArgumentException.class, () -> new BoardTextView(wideBoard));
    assertThrows(IllegalArgumentException.class, () -> new BoardTextView(tallBoard));
  }

  private void boardHelper(Board<Character> b, String expectedHeader, String expectedBody, boolean isSelf) {
    BoardTextView view = new BoardTextView(b);
    assertEquals(expectedHeader, view.makeHeader());
    String expected = expectedHeader + expectedBody + expectedHeader;
    if (isSelf) {
      assertEquals(expected, view.displayMyOwnBoard());
    } else {
      assertEquals(expected, view.displayEnemyBoard());
    }
  }

  @Test
  public void test_display_3by4() {
    int w = 4;
    int h = 3;
    BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(w, h, 'X');
    boardHelper(b1, "  0|1|2|3\n", "A  | | |  A\n" + "B  | | |  B\n" + "C  | | |  C\n", true);
    Coordinate c1 = new Coordinate(0, 2);
    Coordinate c2 = new Coordinate(1, 1);
    RectangleShip<Character> s1 = new RectangleShip<Character>(c1, 's', '*');
    RectangleShip<Character> s2 = new RectangleShip<Character>(c2, 's', '*');
    b1.tryAddShip(s1);
    b1.tryAddShip(s2);
    boardHelper(b1, "  0|1|2|3\n", "A  | |s|  A\n" + "B  |s| |  B\n" + "C  | | |  C\n", true);
    boardHelper(b1, "  0|1|2|3\n", "A  | | |  A\n" + "B  | | |  B\n" + "C  | | |  C\n", false);
    Coordinate c3 = new Coordinate(2, 1);
    b1.fireAt(c1);
    b1.fireAt(c3);
    boardHelper(b1, "  0|1|2|3\n", "A  | |*|  A\n" + "B  |s| |  B\n" + "C  | | |  C\n", true);
    boardHelper(b1, "  0|1|2|3\n", "A  | |s|  A\n" + "B  | | |  B\n" + "C  |X| |  C\n", false);
  }

  @Test
  public void test_display_3by5() {
    int w = 5;
    int h = 3;
    BattleShipBoard<Character> b = new BattleShipBoard<Character>(w, h, 'X');
    boardHelper(b, "  0|1|2|3|4\n", "A  | | | |  A\n" + "B  | | | |  B\n" + "C  | | | |  C\n", true);
  }

  //@Disabled
  @Test
  public void test_side_by_side_boards() throws IllegalArgumentException {
    int w = 5;
    int h = 3;
    BattleShipBoard<Character> ownBoard = new BattleShipBoard<Character>(w, h, 'X');
    BattleShipBoard<Character> enemyBoard = new BattleShipBoard<Character>(w, h, 'X');

    Coordinate ownBoardCoordinate1 = new Coordinate(0, 2);
    Coordinate ownBoardCoordinate2 = new Coordinate(1, 1);
    RectangleShip<Character> ownBoardShip1 = new RectangleShip<Character>(ownBoardCoordinate1, 's', '*');
    RectangleShip<Character> ownBoardShip2 = new RectangleShip<Character>(ownBoardCoordinate2, 's', '*');
    ownBoard.tryAddShip(ownBoardShip1);
    ownBoard.tryAddShip(ownBoardShip2);
    ownBoard.fireAt(ownBoardCoordinate1);
    ownBoard.fireAt(new Coordinate(2, 3));

    Coordinate enemyBoardCoordinate1 = new Coordinate(1, 4);
    Coordinate enemyBoardCoordinate2 = new Coordinate(2, 2);
    RectangleShip<Character> enemyBoardShip1 = new RectangleShip<Character>(enemyBoardCoordinate1, 's', '*');
    RectangleShip<Character> enemyBoardShip2 = new RectangleShip<Character>(enemyBoardCoordinate2, 's', '*');
    enemyBoard.tryAddShip(enemyBoardShip1);
    enemyBoard.tryAddShip(enemyBoardShip2);
    enemyBoard.fireAt(enemyBoardCoordinate2);
    enemyBoard.fireAt(new Coordinate(0, 2));

    BoardTextView ownView = new BoardTextView(ownBoard);
    BoardTextView enemyView = new BoardTextView(enemyBoard);
    String expected = "     Your ocean                                Enemy's Ocean\n" +
                      "  0|1|2|3|4                                 0|1|2|3|4\n" +
                      "A  | |*| |  A                             A  | |X| |  A\n" +
                      "B  |s| | |  B                             B  | | | |  B\n" +
                      "C  | | | |  C                             C  | |s| |  C\n" +
                      "  0|1|2|3|4                                 0|1|2|3|4\n";
    assertEquals(expected, ownView.displayMyBoardWithEnemyNextToIt(enemyView, "Your ocean", "Enemy's Ocean"));

    BattleShipBoard<Character> differentBoard = new BattleShipBoard<Character>(6, 3, 'X');
    BoardTextView diffView = new BoardTextView(differentBoard);
    //assertThrows(IllegalArgumentException.class, () -> diffView.displayMyBoardWithEnemyNextToIt(enemyView, "a", "b"));
  }
}















