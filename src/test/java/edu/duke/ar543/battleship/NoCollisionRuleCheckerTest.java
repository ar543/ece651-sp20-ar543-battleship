package edu.duke.ar543.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class NoCollisionRuleCheckerTest {
  @Test
  public void test_no_collion_rule() {
    V1ShipFactory factory = new V1ShipFactory();
    NoCollisionRuleChecker<Character> ncrc = new NoCollisionRuleChecker<Character>(null);
    BattleShipBoard<Character> b = new BattleShipBoard<Character>(8, 10, ncrc, 'X');
    assertEquals(b.tryAddShip(factory.makeSubmarine(new Placement("A3V"))), null);
    assertEquals(b.tryAddShip(factory.makeSubmarine(new Placement("A3V"))),
                 "That placement is invalid: the ship overlaps another ship.\n");
    assertEquals(b.tryAddShip(factory.makeSubmarine(new Placement("B3V"))),
                 "That placement is invalid: the ship overlaps another ship.\n");
    assertEquals(b.tryAddShip(factory.makeSubmarine(new Placement("F6V"))), null);
    assertEquals(b.tryAddShip(factory.makeSubmarine(new Placement("E6V"))),
                 "That placement is invalid: the ship overlaps another ship.\n"); // should be true when horizontal placement is implemented
    assertEquals(b.tryAddShip(factory.makeSubmarine(new Placement("F5V"))), null);  // should be false when horizontal placement is implemented
    assertEquals(b.tryAddShip(factory.makeSubmarine(new Placement("H2V"))), null);
    assertEquals(b.tryAddShip(factory.makeSubmarine(new Placement("G2V"))),
                 "That placement is invalid: the ship overlaps another ship.\n");
  }

  @Test
  public void test_inbounds_and_no_collision() {
    V1ShipFactory factory = new V1ShipFactory();
    NoCollisionRuleChecker<Character> ncrc = new NoCollisionRuleChecker<Character>(null);
    InBoundsRuleChecker<Character> ibrc = new InBoundsRuleChecker<Character>(ncrc);
    BattleShipBoard<Character> b = new BattleShipBoard<Character>(8, 10, ibrc, 'X');
    assertEquals(b.tryAddShip(factory.makeSubmarine(new Placement("A1V"))), null);
    assertEquals(b.tryAddShip(factory.makeSubmarine(new Placement("A2V"))), null);
    assertEquals(b.tryAddShip(factory.makeCarrier(new Placement("E4V"))), null);
    assertEquals(b.tryAddShip(factory.makeCarrier(new Placement("F5V"))),
                 "That placement is invalid: the ship goes off the bottom of the board.\n");
    //assertEquals(b.tryAddShip(factory.makeDestroyer(new Placement("C4V"))), false);
    assertEquals(b.tryAddShip(factory.makeSubmarine(new Placement("C4V"))), null);
    assertEquals(b.tryAddShip(factory.makeBattleship(new Placement("I4V"))),
                 "That placement is invalid: the ship goes off the bottom of the board.\n");
    assertEquals(b.tryAddShip(factory.makeCarrier(new Placement(new Coordinate(-1, 3), 'v'))),
                 "That placement is invalid: the ship goes off the top of the board.\n");
    assertEquals(b.tryAddShip(factory.makeSubmarine(new Placement(new Coordinate(3, 9), 'v'))),
                 "That placement is invalid: the ship goes off the right of the board.\n");
  }
}







