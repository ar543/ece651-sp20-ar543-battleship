package edu.duke.ar543.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class PlacementTest {
  @Test
  public void test_orientation() {
    Placement p1 = new Placement("A4V");
    Placement p2 = new Placement("A4H");
    assertEquals(p1.getCoordinate().toString(), "(0, 4)");
    assertEquals(p1.getOrientation(), 'V');
    assertEquals(p2.getOrientation(), 'H');
    assertThrows(IllegalArgumentException.class, () -> new  Placement("A4"));
    assertThrows(IllegalArgumentException.class, () -> new  Placement("A49"));
    assertThrows(IllegalArgumentException.class, () -> new  Placement("A4a"));
  }

  @Test
  public void test_to_string() {
    Placement p = new Placement("C9H");
    assertEquals("[(2, 9), H]", p.toString());
  }

  @Test
  public void test_equals () {
    Placement p1 = new Placement("C5V");
    Placement p2 = new Placement("C5V");
    Placement p3 = new Placement("B7H");
    Placement p4 = new Placement("B7h");
    Placement p5 = new Placement(new Coordinate("B7"), 'H');
    
    // test whether or not objects are equal
    assertEquals(p1, p1);
    assertEquals(p1, p2);
    assertNotEquals(p1, p3);
    assertNotEquals(p2, p3);

    // test .equals() method
    char c = 'a';
    assertNotEquals(p1.equals(c), true);
    assertEquals(p3.equals(p4), true);
    assertNotEquals(p1.equals(p3), true);
    assertEquals(p1.equals(p1), true);
    assertEquals(p3.equals(p5), true);
  }

  @Test
  public void test_hashcode() {
    Placement p1 = new Placement("C5V");
    assertEquals(-496688152, p1.hashCode());
  }
}












