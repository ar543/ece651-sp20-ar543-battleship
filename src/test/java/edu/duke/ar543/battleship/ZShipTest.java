package edu.duke.ar543.battleship;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;

import org.junit.jupiter.api.Test;

public class ZShipTest {
  @Test
  public void test_constructor() {
    HashMap<Integer, Coordinate> expectedUp = new HashMap<Integer, Coordinate>();
    expectedUp.put(1, new Coordinate(1,3));
    expectedUp.put(2, new Coordinate(2,3));
    expectedUp.put(3, new Coordinate(3,3));
    expectedUp.put(4, new Coordinate(3,4));
    expectedUp.put(5, new Coordinate(4,4));
    expectedUp.put(6, new Coordinate(5,4));
    ZShip<Character> up = new ZShip<Character>("Carrier", new Coordinate(1,3), 'U', 'c', '*');
    assertEquals(expectedUp, up.getIntToPiecesMap());
    
    HashMap<Integer, Coordinate> expectedRight = new HashMap<Integer, Coordinate>();
    expectedRight.put(1, new Coordinate(4,7));
    expectedRight.put(2, new Coordinate(4,6));
    expectedRight.put(3, new Coordinate(4,5));
    expectedRight.put(4, new Coordinate(5,5));
    expectedRight.put(5, new Coordinate(5,4));
    expectedRight.put(6, new Coordinate(5,3));
    ZShip<Character> right = new ZShip<Character>("Carrier", new Coordinate(4,3), 'R', 'c', '*');
    assertEquals(expectedRight, right.getIntToPiecesMap());
    
    HashMap<Integer, Coordinate> expectedDown = new HashMap<Integer, Coordinate>();
    expectedDown.put(1, new Coordinate(6,5));
    expectedDown.put(2, new Coordinate(5,5));
    expectedDown.put(3, new Coordinate(4,5));
    expectedDown.put(4, new Coordinate(4,6));
    expectedDown.put(5, new Coordinate(3,6));
    expectedDown.put(6, new Coordinate(2,6));
    ZShip<Character> down = new ZShip<Character>("Carrier", new Coordinate(2,5), 'd', 'c', '*');
    assertEquals(expectedDown, down.getIntToPiecesMap());
    
    HashMap<Integer, Coordinate> expectedLeft = new HashMap<Integer, Coordinate>();
    expectedLeft.put(1, new Coordinate(6,6));
    expectedLeft.put(2, new Coordinate(6,5));
    expectedLeft.put(3, new Coordinate(6,4));
    expectedLeft.put(4, new Coordinate(5,4));
    expectedLeft.put(5, new Coordinate(5,3));
    expectedLeft.put(6, new Coordinate(5,2));
    ZShip<Character> left = new ZShip<Character>("Carrier", new Coordinate(5,2), 'l', 'c', '*');
    assertEquals(expectedLeft, left.getIntToPiecesMap());

    assertThrows(IllegalArgumentException.class, () -> new ZShip<Character>("Carrier", new Coordinate(2,4), 'f', 'c', '*'));
  }

  @Test
  public void test_get_starting_coordinate() {
    Coordinate c = new Coordinate(1,2);
    ZShip<Character> up = new ZShip<Character>("Carrier", c, 'U', 'c', '*');
    ZShip<Character> right = new ZShip<Character>("Carrier", c, 'R', 'c', '*');
    ZShip<Character> down = new ZShip<Character>("Carrier", c, 'D', 'c', '*');
    ZShip<Character> left = new ZShip<Character>("Carrier", c, 'L', 'c', '*');

    assertEquals(c,up.getStartingCoordinate());
    assertEquals(c,right.getStartingCoordinate());
    assertEquals(c,down.getStartingCoordinate());
    assertEquals(c,left.getStartingCoordinate());
  }

}






