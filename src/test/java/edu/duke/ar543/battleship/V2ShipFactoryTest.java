package edu.duke.ar543.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class V2ShipFactoryTest {
  @Test
  public void test_make_battleship() {
    V2ShipFactory factory = new V2ShipFactory();
    Placement p = new Placement(new Coordinate(1,3), 'u');
    BasicShip<Character> battleship = (BasicShip<Character>)factory.makeBattleship(p);
    BasicShip<Character> expected = new TShip<Character>("Battleship", p.getCoordinate(), p.getOrientation(), 'b', '*');
    assertEquals(expected.getName(), battleship.getName());
    assertEquals(expected.getIntToHitsMap(), battleship.getIntToHitsMap());
    assertEquals(expected.getIntToPiecesMap(), battleship.getIntToPiecesMap());
  }
}











