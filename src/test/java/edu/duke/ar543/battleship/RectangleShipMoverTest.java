package edu.duke.ar543.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

public class RectangleShipMoverTest {
  @Test
  public void test_move_ship() throws IllegalArgumentException {
    BattleShipBoard<Character> b = new BattleShipBoard<Character>(10, 15, 'X');
    V1ShipFactory factory = new V1ShipFactory();
    Ship<Character> submarine = factory.makeSubmarine(new Placement(new Coordinate(0,0), 'v'));
    Ship<Character> destroyer = factory.makeDestroyer(new Placement(new Coordinate(2,4), 'v'));
    Ship<Character> battleship = factory.makeBattleship(new Placement(new Coordinate(7,3), 'h'));
    Ship<Character> carrier = factory.makeCarrier(new Placement(new Coordinate(8,0), 'h'));
    b.tryAddShip(submarine);
    b.tryAddShip(destroyer);
    b.tryAddShip(battleship);
    b.tryAddShip(carrier);

    BoardTextView view = new BoardTextView(b);
    RectangleShipMover<Character> mover = new RectangleShipMover<Character>(b);
    assertEquals('d', b.whatIsAt(new Coordinate(2,4), true));
    assertEquals('d', b.whatIsAt(new Coordinate(3,4), true));
    assertEquals('d', b.whatIsAt(new Coordinate(4,4), true));
    mover.moveShip((BasicShip<Character>)destroyer, new Placement("C0h"));
    assertEquals(null, b.whatIsAt(new Coordinate(2,4), true));
    assertEquals(null, b.whatIsAt(new Coordinate(3,4), true));
    assertEquals(null, b.whatIsAt(new Coordinate(4,4), true));
    assertEquals('d', b.whatIsAt(new Coordinate(2,0), true));
    assertEquals('d', b.whatIsAt(new Coordinate(2,1), true));
    assertEquals('d', b.whatIsAt(new Coordinate(2,2), true));

    //assertEquals("", view.displayMyOwnBoard());
    
    assertEquals('b', b.whatIsAt(new Coordinate(7,3), true));
    assertEquals('b', b.whatIsAt(new Coordinate(7,4), true));
    assertEquals('b', b.whatIsAt(new Coordinate(7,5), true));
    assertEquals('b', b.whatIsAt(new Coordinate(7,6), true));
    mover.moveShip((BasicShip<Character>)battleship, new Placement("E3V"));
    assertEquals('b', b.whatIsAt(new Coordinate(7,3), true));
    assertEquals(null, b.whatIsAt(new Coordinate(7,4), true));
    assertEquals(null, b.whatIsAt(new Coordinate(7,5), true));
    assertEquals('b', b.whatIsAt(new Coordinate(4,3), true));
    assertEquals('b', b.whatIsAt(new Coordinate(5,3), true));

    assertThrows(IllegalArgumentException.class, () -> mover.moveShip((BasicShip<Character>)battleship, new Placement("G2V")));
    assertThrows(IllegalArgumentException.class, () -> mover.moveShip((BasicShip<Character>)battleship, new Placement("G2D")));
  }

}







