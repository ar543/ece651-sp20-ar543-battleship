package edu.duke.ar543.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import org.junit.jupiter.api.Test;

public class RectangleShipTest {
  @Test
  public void test_make_coords() {
    Coordinate c1 = new Coordinate(1,  2);
    Coordinate c2 = new Coordinate(2,  2);
    Coordinate c3 = new Coordinate(3,  2);
    Coordinate c4 = new Coordinate(4,  2);
    HashMap<Integer, Coordinate> expected = new HashMap<Integer, Coordinate>();
    expected.put(1, c1);
    expected.put(2, c2);
    expected.put(3, c3);
    expected.put(4, c4);
    HashMap<Integer, Coordinate> actual = RectangleShip.makeCoords(c1, 1, 4);
    assertEquals(true, expected.equals(actual));

    Coordinate c5 = new Coordinate(1,  2);
    Coordinate c6 = new Coordinate(1,  3);
    Coordinate c7 = new Coordinate(2,  2);
    Coordinate c8 = new Coordinate(2,  3);
    HashMap<Integer, Coordinate> expected2 = new HashMap<Integer, Coordinate>();
    expected2.put(1, c5);
    expected2.put(2, c6);
    expected2.put(3, c7);
    expected2.put(4, c8);
    HashMap<Integer, Coordinate> actual2 = RectangleShip.makeCoords(c5, 2, 2);
    assertEquals(true, expected2.equals(actual2));
  }

  @Test
  public void test_rectangle_ship() {
    Coordinate c1 = new Coordinate(1,  2);
    Coordinate c2 = new Coordinate(2,  2);
    Coordinate c3 = new Coordinate(3,  2);
    Coordinate c4 = new Coordinate(4,  2);
    HashMap<Integer, Coordinate> expected = new HashMap<Integer, Coordinate>();
    expected.put(1, c1);
    expected.put(2, c2);
    expected.put(3, c3);
    expected.put(4, c4);
    RectangleShip<Character> ship1 = new RectangleShip<Character>("submarine", c1, 1, 4, 's', '*');
    for (Coordinate c  : expected.values()) {
      assertEquals(ship1.occupiesCoordinates(c), true);
    }

    HashMap<Integer, Coordinate> idToPieces = ship1.getIntToPiecesMap();
    for (int i = 1; i <= 4; i++) {
      assertEquals(expected.get(i), idToPieces.get(i));
    }
  }

  @Test
  void test_get_coordinates() {
    Coordinate c1 = new Coordinate(1,  2);
    Coordinate c2 = new Coordinate(2,  2);
    Coordinate c3 = new Coordinate(3,  2);
    Coordinate c4 = new Coordinate(4,  2);
    HashMap<Integer, Coordinate> expected = new HashMap<Integer, Coordinate>();
    expected.put(1, c1);
    expected.put(2, c2);
    expected.put(3, c3);
    expected.put(4, c4);
    RectangleShip<Character> ship1 = new RectangleShip<Character>("submarine", c1, 1, 4, 's', '*');
    for (Coordinate c  : expected.values()) {
      assertEquals(ship1.occupiesCoordinates(c), true);
    }

    HashMap<Integer, Coordinate> idToPieces = ship1.getIntToPiecesMap();
    for (int i = 1; i <= 4; i++) {
      assertEquals(expected.get(i), idToPieces.get(i));
    }
    assertEquals(c1, ship1.getStartingCoordinate());
  }

  @Test
  public void test_second_constructor() {
    // create ship
    Coordinate c1 = new Coordinate(1,  2);
    Coordinate c2 = new Coordinate(2,  2);
    Coordinate c3 = new Coordinate(3,  2);
    Coordinate c4 = new Coordinate(4,  2);
    HashMap<Integer, Coordinate> intToCoord = new HashMap<Integer, Coordinate>();
    intToCoord.put(1, c1);
    intToCoord.put(2, c2);
    intToCoord.put(3, c3);
    intToCoord.put(4, c4);
    
    HashMap<Integer, Boolean> intToHit = new HashMap<Integer, Boolean>();
    intToHit.put(1, false);
    intToHit.put(2, true);
    intToHit.put(3, true);
    intToHit.put(4, false);
    RectangleShip<Character> ship = new RectangleShip<Character>("submarine", c1, 1, 4, intToHit, 's', '*');
    HashMap<Integer, Coordinate> coords = ship.getIntToPiecesMap();
    HashMap<Integer, Boolean> hits = ship.getIntToHitsMap();

    assertEquals(intToCoord, coords);
    assertEquals(intToHit, hits);
    // make a set of expected coordinates
    HashSet<Coordinate> expected = new HashSet<Coordinate>();
    expected.add(c1);
    expected.add(c2);
    expected.add(c3);
    expected.add(c4);

    /**
    // get actual coordinates of the ship
    Iterable<Coordinate> actual = ship.getCoordinates();

    // assert that expected and actual coordinates are the same
    Iterator<Coordinate> actualIt = actual.iterator();
    int numActualCoords = 0;
    while (actualIt.hasNext()) {
      numActualCoords += 1;
      Coordinate c = actualIt.next();
      assertEquals(true, expected.contains(c));
    }
    assertEquals(4, numActualCoords);
    */
  }

  @Test
  public void test_hits() {
    Coordinate c1 = new Coordinate(1,  2);
    Coordinate c2 = new Coordinate(2,  2);
    Coordinate c3 = new Coordinate(3,  2);
    Coordinate c4 = new Coordinate(4,  2);
    RectangleShip<Character> ship = new RectangleShip<Character>("submarine", c1, 1, 4, 's', '*');
    ship.recordHitAt(c2);
    assertEquals(ship.wasHitAt(c2), true);
    assertEquals(ship.wasHitAt(c1), false);
    assertThrows(IllegalArgumentException.class, () -> ship.wasHitAt(new Coordinate(2,3)));
  }

  @Test
  public void test_sink() {
    Coordinate c1 = new Coordinate(1,  2);
    Coordinate c2 = new Coordinate(2,  2);
    Coordinate c3 = new Coordinate(3,  2);
    Coordinate c4 = new Coordinate(4,  2);
    RectangleShip<Character> ship = new RectangleShip<Character>("submarine", c1, 1, 4, 's', '*');
    ship.recordHitAt(c1);
    ship.recordHitAt(c2);
    ship.recordHitAt(c3);
    assertEquals(false, ship.isSunk());
    ship.recordHitAt(c4);
    assertEquals(true, ship.isSunk());
  }

  @Test
  public void test_display_info() {
    Coordinate c1 = new Coordinate(1,2);
    Coordinate c2 = new Coordinate(2,2);
    Coordinate c3 = new Coordinate(3,2);
    Coordinate c4 = new Coordinate(4,2);
    RectangleShip<Character> ship = new RectangleShip<Character>("submarine", c1, 1, 4, 's', '*');
    ship.recordHitAt(c1);
    assertEquals('*', ship.displayInfoAt(c1, true));    
    assertEquals('s', ship.displayInfoAt(c2, true));
    assertEquals('s', ship.displayInfoAt(c1, false));
    assertEquals(null, ship.displayInfoAt(c2, false));    
    assertEquals("submarine", ship.getName());
  }
}











