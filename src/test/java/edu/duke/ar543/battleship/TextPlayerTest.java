package edu.duke.ar543.battleship;

import static org.junit.jupiter.api.Assertions.*;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.StringReader;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

public class TextPlayerTest {
  private TextPlayer createTextPlayer(int w, int h, String inputData, OutputStream bytes) {
    BufferedReader input = new BufferedReader(new StringReader(inputData));
    PrintStream output = new PrintStream(bytes, true);
    Board<Character> board = new BattleShipBoard<Character>(w, h, 'X');
    V1ShipFactory shipFactory = new V1ShipFactory();
    return new TextPlayer("A", board, input, output, shipFactory, 3, 3);
  }

  @Test
  public void test_read_placement() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(10, 20, "B2V\nC8H\na4v\n", bytes);

    String prompt = "Please enter a location for a ship:";
    Placement[] expected = new Placement[3];
    expected[0] = new Placement(new Coordinate(1, 2), 'V');
    expected[1] = new Placement(new Coordinate(2, 8), 'H');
    expected[2] = new Placement(new Coordinate(0, 4), 'V');
    for (int i = 0; i < expected.length; i++) {
      Placement p = player.readPlacement(prompt);
      assertEquals(p, expected[i]); // did we get the right Placement back
      // assertEquals(prompt + "\n", bytes.toString()); // should have printed prompt
      // and newline
      bytes.reset(); // clear out bytes for next time around
    }
    ByteArrayOutputStream bytes2 = new ByteArrayOutputStream();
    TextPlayer player1 = createTextPlayer(10, 20, "B2D\n", bytes2);
    Placement p1 = new Placement(new Coordinate(1, 2), 'd');
    assertEquals(p1, player1.readPlacement("Enter placement:\n"));

    TextPlayer player2 = createTextPlayer(10, 20, "B2f\n", bytes2);
    Placement p2 = new Placement(new Coordinate(1, 2), 'd');
    assertThrows(IllegalArgumentException.class, () -> player2.readPlacement("Enter placement:\n"));
  }

  @Test
  public void test_play_choice() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    int w = 10;
    int h = 20;
    BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(w, h, 'X');
    BattleShipBoard<Character> b2 = new BattleShipBoard<Character>(w, h, 'X');
    TextPlayer player1 = createTextPlayer(10, 20, "A0v\nA1v\nA2v\nA3v\nA4v\nA5d\nF0d\nf7d\nk0d\nk6d\ng3\nh5\nd2\ns\ng3\nf\ne2\nm\na0\n3v\nh2\nd4\na1\nn0v\n", bytes);
    TextPlayer player2 = createTextPlayer(10, 20, "A0v\nA1v\nA2v\nA3v\nA4v\nA5d\nF0d\nf7d\nk0d\nk6d\n", bytes);
    char choice1 = 'f';
    char choice2 = 'm';
    char choice3 = 's';

    BoardTextView myView = new BoardTextView(b1);
    BoardTextView enemyView = new BoardTextView(b2);
    assertEquals(true, player1.playChoice(choice1, b2));
    assertEquals(true, player1.playChoice(choice2, b2));
    assertEquals(true, player1.playChoice(choice3, b2));
  }
  
  @Disabled
  @Test
  public void test_placement_phase() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(10, 20, "B2V\n", bytes);
    player.doPlacementPhase();
    String expected = "test";
    assertEquals(expected, player.getView());
    
  }
  
  @Disabled
  @Test
  public void test_do_one_placement() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(5, 4, "A3V\n", bytes);

    // String playerName = "A";
    // String shipName = "Destroyer";
    String expected =
        // "Player " + playerName + " where do you want to place a " + shipName + "?\n"
        // +
        "  0|1|2|3|4\n" + "A  | | |d|  A\n" + "B  | | |d|  B\n" + "C  | | |d|  C\n" + "D  | | | |  D\n"
            + "  0|1|2|3|4\n";
    // player.doOnePlacement();
    assertEquals(expected, bytes.toString());
  }

  @Disabled
  @Test
  public void test_do_placement_phase() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(5, 4, "A3V\n", bytes);

    String playerName = "A";
    String shipName = "Destroyer";
    String expected = "  0|1|2|3|4\n" + "A  | | | |  A\n" + "B  | | | |  B\n" + "C  | | | |  C\n" + "D  | | | |  D\n"
        + "  0|1|2|3|4\n" + "--------------------------------------------------------------------------------\n"
        + "Player A: you are going to place the following ships (which are all\n"
        + "rectangular). For each ship, type the coordinate of the upper left\n"
        + "side of the ship, followed by either H (for horizontal) or V (for\n"
        + "vertical).  For example M4H would place a ship horizontally starting\n"
        + "at M4 and going to the right.  You have\n\n" + "2 \"Submarines\" ships that are 1x2\n"
        + "3 \"Destroyers\" that are 1x3\n" + "3 \"Battleships\" that are 1x4\n" + "2 \"Carriers\" that are 1x6\n"
        + "--------------------------------------------------------------------------------\n" + "  0|1|2|3|4\n"
        + "A  | | |d|  A\n" + "B  | | |d|  B\n" + "C  | | |d|  C\n" + "D  | | | |  D\n" + "  0|1|2|3|4\n";

    player.doPlacementPhase();
    assertEquals(expected, bytes.toString());
  }

  @Disabled
  @Test
  public void test_read_choice() throws IOException {
    BufferedReader input = new BufferedReader(new StringReader("s"));
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(10, 20, "B2V\nC8H\na4v\n", bytes);
    // char choice = player.readChoice(input);
    // assertEquals('S', choice);
  }

  @Test
  public void test_read_coordinate() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(10, 20, "B2\n", bytes);
    Coordinate c = new Coordinate(1, 2);
    assertEquals(c, player.readCoordinate("Enter coordinate:\n"));

    TextPlayer player2 = createTextPlayer(10, 20, "B43\n", bytes);
    assertThrows(IllegalArgumentException.class, () -> player2.readCoordinate("Enter coordinate:\n"));
  }

}
